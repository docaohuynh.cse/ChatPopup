({
    baseUrl: "js/shop",
    appDir: '.',
    paths: {
        "jquery": "jquery",
        "backbone": "backbone",
        "underscore": "underscore"
    },
    shim: {
        "backbone": {
            "deps": ["underscore", "jquery"],
            "exports": "Backbone"  //attaches "Backbone" to the window object
        },
        "underscore": {
            exports: "_" // exporting _
        }
    },
    optimizeCss: "standard.keepLines",
    modules: [
        {
            name: "app"
        }
    ],
    dir: "../built",
})
