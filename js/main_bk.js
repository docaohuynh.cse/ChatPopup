var config;
if (typeof(require) === 'undefined') {
    /* XXX: Hack to work around r.js's stupid parsing.
     * We want to save the configuration in a variable so that we can reuse it in
     * tests/main.js.
     */
    require = { // jshint ignore:line
        config: function (c) {
            config = c;
        }
    };
}
require.config({
    baseUrl: 'js/chat',
    waitSeconds: 20,
    paths: {
        'templates': 'templates',
        "requiremore": "requiremore",
        "jquery": "jquery",
        "cross-jquery": "plugin/jQuery.XDomainRequest",
        "jquery-place": "plugin/jquery.placeholder",
        "backbone": "backbone",
        // "backbone.browserStorage":  "plugin/backbone.browserStorage/backbone.browserStorage",
        "localStorage":  "localStorage",
        "underscore": "underscore",
        "strophe": "strophe",
        "hana-pluggable": "hana-pluggable",
        "finger": "plugin/fingerprint2.min",
        "base64": "plugin/jquery.base64.min",
        "slick": "plugin/slick",
        'text': 'plugin/text',
        'tpl': 'plugin/tpl'
    },
    //for traditional libraries not using define() we need to use a shim that allows us to declare them as AMD modules
    shim: {
        "backbone": {
            "deps": ["underscore", "jquery"]
            // "exports": "Backbone"  //attaches "Backbone" to the window object
        },
        "localStorage": {
            "deps": ["backbone"]
        },
        "underscore": {
            exports: "_" // exporting _
        }
    },

    deps: ['app'],
    urlArgs: "t=20160320000000" //flusing cache, do not use in production
});

