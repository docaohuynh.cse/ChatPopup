// hanaCore.js (A browser based XMPP chat client)
// http://hanaCorejs.org
//
// Copyright (c) 2012-2016, Jan-Carel Brand <jc@opkode.com>
// Licensed under the Mozilla Public License (MPLv2)
//
/*global define */

/* This is a hanaCore.js plugin which add support for application-level pings
 * as specified in XEP-0199 XMPP Ping.
 */
(function (root, factory) {
    define("hana-create-room", [
        "hana-core",
        "hana-api",
    ], factory);
}(this, function (hanaCore, hana_api) {
    "use strict";
    // Strophe methods for building stanzas
    var Strophe = hana_api.env.Strophe;
    // Other necessary globals
    var ha_ = hana_api.env.ha_,
    ha$iq = hana_api.env.$iq,
    ha$build = hana_api.env.$build,
    ha$msg = hana_api.env.$msg,
    ha$pres = hana_api.env.$pres,
    ha$ = hana_api.env.jQuery;


    hana_api.plugins.add('hana-create-room', {

        initialize: function () {

            hanaCore.createChatRoom = {
                create: function (data) {
                    //console_bk.log('ONCONNECTED createChatRoom ');
                    data = hanaCore.dataObject.list_room;
                    var that = this;
                    if (data) {
                        var onlineUser, supporter;
                        var json = data;
                        var useragent = [];
                        // console_bk.log('BEFOR ' + JSON.stringify(hanaCore.all_agent.users));
                        if (hanaCore.list_agent_chat) {
                            ha_.each(hanaCore.list_agent_chat, function (stringchat) {
                                var us = ha_.find(hanaCore.all_agent.users, function (us) {
                                    return us.username === stringchat;
                                });
                                useragent.push(us);
                            });
                        } else {
                            useragent = hanaCore.all_agent.users;
                        }
                        // console.log('AFTER ' + JSON.stringify(useragent));
                        var agentOnline =  ha_.filter(useragent, function(us){  return us && us.lastseen == '0'; });
                        var agentOffline =  ha_.filter(useragent, function(us){ return us && us.lastseen != '0'; });
                        if (agentOnline.length === 0) { // khong co thang nao online

                        } else { // tim trong nhung thang online

                        }
                        if (json.code == 200) {
                            hanaCore.log("Exist room");
                            hanaCore.newsession = json.newsession;
                            if (json.newsession == 'true') {
                                hanaCore.log("[HUYNHDC] them tat ca agent+ chatbot vao");
                                var iq = ha$iq({ to: json.room + hanaCore.hana_room, type: "set", id: Math.random().toString(36).substr(2, 6) })
                                    .c("query", { xmlns: "invite" });
                                ha_.each(hanaCore.all_agent.users, function(userx) {
                                    hanaCore.log(userx.username);
                                    hanaCore.log(userx.name);
                                    var itemmember = ha$build("member", { jid: userx.username, role: "member" });
                                    iq = iq.cnode(itemmember.node).up();
                                });

                                var itemmember = ha$build("member", { jid: hanaCore.all_agent.hana, role: "agent" });
                                iq = iq.cnode(itemmember.node).up();
                                var itemqueue = ha$build("queue", { exchange: hanaCore.all_agent.queue, key: hanaCore.all_agent.queue, is_pick: 0 });
                                iq = iq.cnode(itemqueue.node).up();
                                hanaCore.connection.sendIQ(iq, that.inviteRoomSuccess.bind(that), that.inviteRoomFail.bind(that));
                            }
                            // kick hana old
                            if (json.hanachange == 1) {
                                //kick old hana
                                var iqkick = ha$iq({ to: json.room + hanaCore.hana_room, type: "set", id: Math.random().toString(36).substr(2, 6) })
                                    .c("query", { xmlns: "kickmember" });
                                var itemmember = ha$build("member", { jid: json.hanaold, role: "agent" });
                                iqkick = iqkick.cnode(itemmember.node).up();
                                hanaCore.connection.sendIQ(iqkick, that.kickRoomSuccess.bind(that, json.is_pick), that.kickRoomFail.bind(that));
                            }
                            that.restoreRoomSuccess(json.room, json.rate, json.is_block);
                            that.addNewSupporter(json, agentOnline, json.deparment_id);

                        } else {
                            if (agentOnline.length === 0) { //khong thang nao online lay thang offline
                                onlineUser = ha_.sample(agentOffline, 1);
                                hanaCore.log('AGENTOFFLINE ' + JSON.stringify(agentOffline) +  ' ONLINE ' + JSON.stringify(onlineUser));
                                supporter = onlineUser[0].username;
                            } else {
                                onlineUser = ha_.sample(agentOnline, 1);
                                supporter = onlineUser[0].username;
                            }

                            hanaCore.newsession = "true";
                            hanaCore.log("Create new room");
                            var item = ha$build("room", { name: "chatweb" });
                            var iq = ha$iq({ to: "create" + hanaCore.hana_room, type: "set", id: Math.random().toString(36).substr(2, 6), entry: "groupchat",appid: hanaCore.all_agent.app, creater: hanaCore.all_agent.uid, supporter: supporter })
                                .c("query", { xmlns: "create" })
                                .cnode(item.node).up();
                            ha_.each(hanaCore.all_agent.users, function(userx) {
                                hanaCore.log(userx.username);
                                hanaCore.log(userx.name);
                                var itemmember = ha$build("member", { jid: userx.username, role: "member" });
                                iq = iq.cnode(itemmember.node).up();
                            });

                            var itemmember = ha$build("member", { jid: hanaCore.all_agent.hana, role: "agent" });
                            iq = iq.cnode(itemmember.node).up();
                            var itemqueue = ha$build("queue", { exchange: hanaCore.all_agent.queue, key: hanaCore.all_agent.queue });
                            iq = iq.cnode(itemqueue.node).up();

                            hanaCore.log('[HUYNHDC create IQ room IIIIIIIIIIIIII2 ]  ' + iq);
                            hanaCore.connection.sendIQ(iq, that.createRoomSuccess.bind(that), that.createRoomFail.bind(that));
                        }
                    }
                },
                addNewSupporter: function(json, agentOnline, deparment) {
                    var supporter, onlineUser;
                    supporter = json.supporter;
                    var supporterIsOnline =  ha_.filter(hanaCore.all_agent.users, function(us){ return (us.username == supporter && us.lastseen == '0') });
                    if (supporterIsOnline.length === 0) { // thang support khong online
                        if (agentOnline.length === 0) { //khong thang nao online khong lam gi

                        } else {
                            if (deparment) {
                                agentOnline = ha_.filter(agentOnline, function(us){  return us && us.deparment_id == parseInt(deparment); });
                                if (agentOnline.length === 0) {
                                    return false;
                                }
                            }
                            onlineUser = ha_.sample(agentOnline, 1);
                            var infoUser = JSON.stringify({ "name": json.room , "supporter": onlineUser[0].username});
                            hanaCore.updateMucRoom(infoUser, function () {
                            });
                        }
                    } else { // online khong lam gi ca

                    }
                },
                inviteRoomSuccess: function(iq) {
                    return this;
                },
                inviteRoomFail: function(iq) {
                    return this;
                },
                kickRoomSuccess: function(is_pick, iq) {
                    var jid = ha$(iq).attr('from');
                    var roomname = Strophe.getNodeFromJid(jid);
                    //invite new hana
                    var iqinvite = ha$iq({ to: roomname + hanaCore.hana_room, type: "set", id: Math.random().toString(36).substr(2, 6) })
                        .c("query", { xmlns: "invite" });
                    var itemmember = ha$build("member", { jid: hanaCore.all_agent.hana, role: "agent" });
                    iqinvite = iqinvite.cnode(itemmember.node).up();
                    var itemqueue = ha$build("queue", { exchange: hanaCore.all_agent.queue, key: hanaCore.all_agent.queue, is_pick: is_pick });
                    iqinvite = iqinvite.cnode(itemqueue.node).up();
                    hanaCore.connection.sendIQ(iqinvite, this.kickRoomFail.bind(this), this.kickRoomFail.bind(this));
                    return this;
                },
                kickRoomFail: function(iq) {
                    return this;
                },
                createRoomFail: function(error) {
                    return this;
                },
                restoreRoomSuccess: function(iq, rate, is_block) {
                    //console_bk.log('restoreRoomSuccess');
                    var d = new Date();
                    var n = d.getTime();
                    var newArgs = [];
                    var jid = iq + hanaCore.hana_room;
                    newArgs.push(jid);
                    newArgs.push(rate);
                    newArgs.push(is_block);
                    // cho nay se tao room chat
                    // this._super.restoreRoomSuccess.apply(this, newArgs);
                    //console_bk.log('restoreRoomSuccess ' + iq);
                    var infoUser = JSON.stringify({ "room_name": iq });
                    //post data to
                    hanaCore.updateActiveContact(iq, function (data) {});
                    hanaCore.updateCustomerInfo(iq, function (data) {});
                    hanaCore.roomId = iq;

                    hanaCore.all_step.step_connected = true;
                    //console_bk.log('CALL EMIT ROOM restoreRoomSuccess');
                    hanaCore.emit("allDone");
                    // hanaCore.chatAction.openChatRoom(iq);
                },
                createRoomSuccess: function(iq) {
                    //console_bk.log('createRoomSuccess');
                    var d = new Date();
                    var n = d.getTime();
                    var jid = ha$(iq).attr('from');
                    var roomname = Strophe.getNodeFromJid(jid);
                    // cho nay se tao room chat
                    var infoUser = JSON.stringify({ "uid": hanaCore.all_agent.uid, "app": hanaCore.all_agent.app, "room_name": roomname });
                    hanaCore.updateRoom(infoUser, function (data) {});
                    hanaCore.updateActiveContact(roomname, function (data) {});
                    hanaCore.updateCustomerInfo(roomname, function (data) {});
                    hanaCore.roomId = roomname;
                    if (hanaCore.roomId) {
                        if (hanaCore.roomId.length < 50) {
                            return false;
                        }
                    } else {
                        return false;
                    }
                    hanaCore.all_step.step_connected = true;
                    //console_bk.log('CALL EMIT ROOM restoreRoomSuccess');
                    hanaCore.emit("allDone");

                    // hanaCore.chatAction.openChatRoom(roomname);
                },

            };

            var onConnectedCreate = function () {
                // Wrapper so that we can spy on registerPingHandler in tests
                //console_bk.log('CONNECTED onConnectedCreate ' + hanaCore.connection.jid);
                if (hanaCore.firstConnect) {
                    hanaCore.chatRoomView.listenMessage();
                    hanaCore.onConnectedPing();
                    //console_bk.log('donereconnected...');
                } else {
                    hanaCore.onConnectedChat();
                    hanaCore.onConnectedPing();
                    hanaCore.createChatRoom.create();
                }
                return;
            };

            hanaCore.on('connected', onConnectedCreate);
        }
    });
}));
