// hanaCore.js (A browser based XMPP chat client)
// http://hanaCorejs.org
//
// Copyright (c) 2012-2016, Jan-Carel Brand <jc@opkode.com>
// Licensed under the Mozilla Public License (MPLv2)
//
/*global define */

/* This is a hanaCore.js plugin which add support for application-level pings
 * as specified in XEP-0199 XMPP Ping.
 */
(function (root, factory) {
    define("hana-chat", [
        "hana-core",
        "hana-api",
        "hana-service",
        "hana-util",
        "hana-create-room",
        "tpl!templates/chat",
        "tpl!templates/headpopup",
        "tpl!templates/bodychat",
        "tpl!templates/bodypopup",
        "tpl!templates/bodynotifypopup",
        "tpl!templates/chatroomheader",
        "tpl!templates/chatroomfooter",
        "tpl!templates/chatroombody",
        "tpl!templates/msgtextme",
        "tpl!templates/msgtexttheir",
        "tpl!templates/msgtextnotifytheir",
        "tpl!templates/msgimageme",
        "tpl!templates/msgimagetheir",
        "tpl!templates/msgquick",
        "tpl!templates/msggallery",
        "tpl!templates/msgcard",
        "tpl!templates/msgbutton",
        "tpl!templates/msgfileme",
        "tpl!templates/msgfiletheir",
        "tpl!templates/headcollapse",
        "tpl!templates/bodycollapse",
        "tpl!templates/chatformbody",
        "tpl!templates/typing",
        "hanajquery-place"
    ], factory);
}(this, function (hanaCore, hana_api, hana_service, hana_util, hana_create_room, tpl_chat, tpl_headpopup, tpl_bodychat, tpl_bodypopup, tpl_bodynotifypopup,
    tpl_chatroomheader, tpl_chatroomfooter, tpl_chatroombody, tpl_msgtextme, tpl_msgtexttheir, tpl_msgtextnotifytheir, tpl_msgimageme, tpl_msgimagetheir,
    tpl_msgquick, tpl_msggallery, tpl_msgcard, tpl_msgbutton, tpl_msgfileme, tpl_msgfiletheir, tpl_headcollapse, tpl_bodycollapse, tpl_chatformbody, tpl_typing) {
    "use strict";
    // Strophe methods for building stanzas
    var Strophe = hana_api.env.Strophe;
    // Other necessary globals
    var ha_ = hana_api.env.ha_,
        ha$iq = hana_api.env.$iq,
        ha$build = hana_api.env.$build,
        ha$msg = hana_api.env.$msg,
        ha$pres = hana_api.env.$pres,
        ha$ = hana_api.env.jQuery;

    hana_api.plugins.add('hana-chat', {

        initialize: function () {
            /* The initialize function gets called as soon as the plugin is
             * loaded by hanaCore.js's plugin machinery.
             */
            // var hanaCore = this.hanaCore;
            //console_bk.log("ADDPLUGIN hana-chat ...");
            this.updateSettings({
                ping_interval: 30 //in seconds
            });
            var KEY = {
                ENTER: 13,
                FORWARD_SLASH: 47
            };
            ha$('body').append(
                tpl_bodychat({
                    heading: 'CHAO'
                }));

            hanaCore.all_step = {
                step_connected: false,
                step_get_bot: false,
                step_integration: false,
                step_list_room: false,
                step_getcustomer: false
            };
            hanaCore.stateVisible = true;
            hanaCore.HeaderModel = Backbone.Model.extend({
                // browserStorage: new Backbone.BrowserStorage[hanaCore.storage](
                //     hanaCore.base64Encode('hana.HeaderModel')),
                localStorage: new Backbone.LocalStorage(hanaCore.base64Encode('hana.HeaderModel')),
                initialize: function (attributes, options) {
                    this.on('destroy', function () { this.removeHeader(); }.bind(this));
                    this.on('change:chat_status', function (item) {
                    });
                },
                removeHeader: function () {
                }

            });
            hanaCore.FooterModel = Backbone.Model.extend({
                // browserStorage: new Backbone.BrowserStorage[hanaCore.storage](
                //     hanaCore.base64Encode('hana.FooterModel')),
                localStorage: new Backbone.LocalStorage(hanaCore.base64Encode('hana.FooterModel')),
                initialize: function (attributes, options) {
                    this.on('destroy', function () { this.removeHeader(); }.bind(this));
                    this.on('change:chat_status', function (item) {
                    });
                },
                removeHeader: function () {
                }
            });
            hanaCore.broadCastMsg = function (data) {
                var dataSend = {type: "popupaction",member: username,entry: this.model.get('entry')};
                var iq = ha$iq({
                    to: hanaCore.roomId + hanaCore.hana_room,
                    type: "set",
                    id: Math.random().toString(36).substr(2, 6)
                })
                    .c("query", {
                        xmlns: "broadcast"
                    }).t(hanaCore.base64Encode(JSON.stringify(dataSend)));
                hanaCore.connection.sendIQ(iq, null, null);
            };
            hanaCore.Message = Backbone.Model.extend({
                // browserStorage: new Backbone.BrowserStorage[hanaCore.storage](
                //     hanaCore.base64Encode('hana.Message'))
                localStorage: new Backbone.LocalStorage(hanaCore.base64Encode('hana.Message'))
            });
            hanaCore.Messages = Backbone.Collection.extend({
                // browserStorage: new Backbone.BrowserStorage[hanaCore.storage](
                //     hanaCore.base64Encode('hana.Messages')),
                localStorage: new Backbone.LocalStorage(hanaCore.base64Encode('hana.Messages')),
                model: hanaCore.Message
            });
            hanaCore.ChatBox = Backbone.Model.extend({
                // browserStorage: new Backbone.BrowserStorage[hanaCore.storage](
                //     hanaCore.base64Encode('hana.ChatBox')),
                localStorage: new Backbone.LocalStorage(hanaCore.base64Encode('hana.ChatBox')),
                initialize: function () {
                    this.messages = new hanaCore.Messages();
                },
                createMessageWelcome: function (message, timesend) {
                    return this.messages.create(this.getMessageWelcomeAttributes.apply(this, arguments));
                },
                createMessage: function (ha$message, ha$delay, original_stanza) {
                    return this.messages.create(this.getMessageAttributesRaw.apply(this, arguments));
                },
                createMessageStore: function (ha$message, ha$delay, original_stanza, timereceive, sort) {
                    return this.messages.create(this.getMessageAttributesStore.apply(this, arguments));
                },
                getMessageWelcomeAttributes: function (message, timesend) {
                    var body = hanaCore.base64Encode(message);
                    var attachment = hanaCore.base64Encode(message);
                    var pattern = 'attachment';
                    var fullname = hanaCore.sky_ai_name;
                    return {
                        'type': 'groupchat',
                        'chat_state': '',
                        'delayed': '',
                        'fullname': fullname,
                        'message': body || undefined,
                        'pattern': pattern,
                        'pathurl': '',
                        'filename': '',
                        'msgid': 'msgid',
                        'sender': 'them',
                        'timesend': timesend,
                        'timesendorg': timesend,
                        'suggestions': '',
                        'attachment': attachment,
                        'sendtype': 'receive',
                        'time': timesend
                    };
                },
                /*getMessageAttributes: function (ha$message, ha$delay, original_stanza) {
                    ha$delay = ha$delay || ha$message.find('delay');
                    var type = ha$message.attr('type'),
                        body, suggestions, stamp, time, sender, from, pattern, pathurl, filename, attachment;
                    if (type === 'error') {
                        body = ha$message.find('error').children('text').text();
                        pattern = 'error';
                    } else if (type === 'chat' || type === 'groupchat') {
                        if (ha$message.find('body').length > 0) {
                            body = ha$message.children('body').text();
                            pattern = 'text';
                        } else if (ha$message.find('attachment').length > 0) {
                            attachment = ha$message.find('attachment').text();
                            var attachtext = JSON.parse(this.base64Decode(attachment));
                            if (attachtext.attachment.type == 'image') {
                                pattern = attachtext.attachment.type;
                                pathurl = attachtext.attachment.payload.url;
                                filename = attachtext.attachment.payload.name;
                            } else if (attachtext.attachment.type == 'file') {
                                pattern = attachtext.attachment.type;
                                pathurl = attachtext.attachment.payload.url;
                                filename = attachtext.attachment.payload.name;
                            }
                        } else if (ha$message.find('frontendstructure').length > 0) {
                            pattern = 'attachment';
                            attachment = ha$message.children('frontendstructure').text();
                        } else {
                            body = ha$message.text();
                        }
                    } else {
                        body = "";
                    }
                    var delayed = ha$delay.length > 0,
                        fullname = this.get('fullname'),
                        is_groupchat = type === 'groupchat';

                    if (is_groupchat) {
                        from = ha$message.attr('member');
                        if (!from) {
                            from = Strophe.getBareJidFromJid(ha$message.attr('from'));
                        } else {
                            if (from.indexOf(hanaCore.sky_hanaprefix) !== -1) {
                                var jidSend = hanaCore.sky_ai_name;
                                fullname = jidSend;
                                from = jidSend;
                            }
                            ha_.each(hanaCore.all_agent.users, function (userx) {
                                if (userx.username == ha$message.attr('member')) {
                                    from = userx.name;
                                    fullname = userx.name;
                                }
                            });
                        }
                    } else {
                        from = Strophe.getBareJidFromJid(ha$message.attr('from'));
                    }
                    if (ha_.isEmpty(fullname)) {
                        fullname = from;
                    }
                    if (delayed) {
                        stamp = ha$delay.attr('stamp');
                        time = stamp;
                    } else {
                    }

                    if ((is_groupchat && from === this.get('nick') || (is_groupchat && from === hanaCore.bare_jid)) || (!is_groupchat && from === hanaCore.bare_jid)) {
                        sender = hanaCore.sky_myname;
                    } else {
                        sender = 'them';
                    }
                    var timeS = ha$message.attr('timesend');
                    var timesend = ha$message.attr('timesend');
                    var timereceive = ha$message.attr('timereceive');

                    var timesendlocal = Number(Date.now());
                    if (isNaN(timesendlocal)) {
                        timesendlocal = Number(Date.now());
                    }
                    hanaCore.log("TIMESEND==TIMERECEIVE==TIMELOCAL " + timesend + "==" + timereceive + "==" + timesendlocal);
                    if (!timeS) {
                    } else {
                        var date = new Date(Number(timeS));
                        time = date;
                    }
                    return {
                        'type': type,
                        'chat_state': '',
                        'delayed': delayed,
                        'fullname': fullname,
                        'message': body || undefined,
                        'pattern': pattern,
                        'pathurl': pathurl,
                        'filename': filename,
                        'msgid': ha$message.attr('id'),
                        'sender': sender,
                        'timesend': timesendlocal,
                        'timesendorg': timesend,
                        'suggestions': suggestions,
                        'attachment': attachment,
                        'sendtype': 'receive',
                        'time': time
                    };
                },*/
                getMessageAttributesRaw: function (ha$message, ha$delay, original_stanza) {
                    // console_bk.log('Attraww ' + original_stanza);
                    var stanza = original_stanza;
                    var xml = ha$.parseXML(stanza);
                    var ha$xml = ha$(xml);
                    var ha$message = ha$(stanza),
                        ha$delay = ha$delay || ha$message.find('delay');
                    var type = ha$message.attr('type'),
                        body, stamp, time, sender, from, pattern, pathurl, filename, attachment, suggestions;
                    if (type === 'error') {
                        body = ha$xml.find('error').children('text').text();
                        pattern = 'error';
                    } else if (type === 'chat' || type === 'groupchat') {
                        if (ha$xml.find('body').length > 0) {
                            body = ha$xml.find('body').text();
                            pattern = 'text';
                        } else if (ha$xml.find('image').length > 0) {
                            pattern = 'image';
                            pathurl = ha$xml.find('pathurl').text();
                            filename = ha$xml.find('name').text();
                            attachment = ha$xml.find('attachment').text();
                        } else if (ha$xml.find('file').length > 0) {
                            pattern = 'file';
                            pathurl = ha$message.find('pathurl').text();
                            filename = ha$message.find('name').text();
                            attachment = ha$message.find('attachment').text();
                        } else if (ha$xml.find('attachment').length > 0) {
                            attachment = ha$xml.find('attachment').text();
                            var attachtext = JSON.parse(this.base64Decode(attachment));
                            if (attachtext.attachment.type == 'image') {
                                pattern = attachtext.attachment.type;
                                pathurl = attachtext.attachment.payload.url;
                                filename = attachtext.attachment.payload.name;
                            } else if (attachtext.attachment.type == 'file') {
                                pattern = attachtext.attachment.type;
                                pathurl = attachtext.attachment.payload.url;
                                filename = attachtext.attachment.payload.name;
                            }
                        } else if (ha$xml.find('frontendstructure').length > 0) {
                            pattern = 'attachment';
                            attachment = ha$xml.find('frontendstructure').text();
                        } else if (ha$xml.find('trigger').length > 0) {
                            body = ha$xml.find('trigger').text();
                            pattern = 'trigger';
                        } else if (ha$xml.find('broadcast').length > 0) {
                            body = ha$xml.find('broadcast').text();
                            pattern = 'broadcast';
                        } else {
                            body = ha$message.text();
                        }
                    } else {
                        body = "";
                    }
                    var fullname = this.get('fullname'),
                        is_groupchat = type === 'groupchat';


                    if (is_groupchat) {
                        from = Strophe.getBareJidFromJid(ha$message.attr('from'));

                        if (!from) {
                            from = Strophe.getBareJidFromJid(ha$message.attr('from'));
                        } else {
                            if (from.indexOf(hanaCore.sky_hanaprefix) !== -1) {
                                var jidSend = hanaCore.sky_ai_name;
                                fullname = jidSend;
                                from = jidSend;
                            }
                            ha_.each(hanaCore.all_agent.users, function (userx) {
                                if (userx.username == Strophe.getNodeFromJid(ha$message.attr('from'))) {
                                    from = userx.name;
                                    fullname = userx.name;
                                }
                            });
                        }
                    } else {
                        from = Strophe.getBareJidFromJid(ha$message.attr('from'));
                    }
                    if (ha_.isEmpty(fullname)) {
                        fullname = from;
                    }

                    if ((is_groupchat && from === this.get('nick') || (is_groupchat && from === hanaCore.bare_jid)) || (!is_groupchat && from === hanaCore.bare_jid)) {
                        sender = hanaCore.sky_myname;
                    } else {
                        sender = 'them';
                    }
                    var timeS = ha$message.attr('timesend');
                    var timesend = ha$message.attr('timesend');
                    var timereceive = ha$message.attr('timereceive');;

                    var timesendlocal = Number(Date.now()) + (Number(timereceive) - Number(timesend));
                    if (!timeS) {

                    } else {
                        var date = new Date(Number(timeS));
                        time = date;
                    }

                    return {
                        'type': type,
                        'chat_state': '',
                        'delayed': '',
                        'fullname': fullname,
                        'message': body || undefined,
                        'pattern': pattern,
                        'pathurl': pathurl,
                        'filename': filename,
                        'msgid': ha$message.attr('id'),
                        'sender': sender,
                        'timesend': timesend,
                        'timesendorg': timesend,
                        'attachment': attachment,
                        'sendtype': 'receive',
                        'time': time
                    };
                },
                getMessageAttributesStore: function (stanza, ha$delay, original_stanza, timereceivestore, sort) {
                    var xml = ha$.parseXML(stanza);
                    var ha$xml = ha$(xml);
                    var ha$message = ha$(stanza),
                        ha$delay = ha$delay || ha$message.find('delay');
                    var type = ha$message.attr('type'),
                        body, stamp, time, sender, from, pattern, pathurl, filename, attachment, suggestions;
                    if (type === 'error') {
                        body = ha$xml.find('error').children('text').text();
                        pattern = 'error';
                    } else if (type === 'chat' || type === 'groupchat') {
                        if (ha$xml.find('body').length > 0) {
                            body = ha$xml.find('body').text();
                            pattern = 'text';
                        } else if (ha$xml.find('image').length > 0) {
                            pattern = 'image';
                            pathurl = ha$xml.find('pathurl').text();
                            filename = ha$xml.find('name').text();
                            attachment = ha$xml.find('attachment').text();
                        } else if (ha$xml.find('file').length > 0) {
                            pattern = 'file';
                            pathurl = ha$message.find('pathurl').text();
                            filename = ha$message.find('name').text();
                            attachment = ha$message.find('attachment').text();
                        } else if (ha$xml.find('attachment').length > 0) {
                            attachment = ha$xml.find('attachment').text();
                            var attachtext = JSON.parse(this.base64Decode(attachment));
                            if (attachtext.attachment.type == 'image') {
                                pattern = attachtext.attachment.type;
                                pathurl = attachtext.attachment.payload.url;
                                filename = attachtext.attachment.payload.name;
                            } else if (attachtext.attachment.type == 'file') {
                                pattern = attachtext.attachment.type;
                                pathurl = attachtext.attachment.payload.url;
                                filename = attachtext.attachment.payload.name;
                            }
                        } else if (ha$xml.find('frontendstructure').length > 0) {
                            pattern = 'attachment';
                            attachment = ha$xml.find('frontendstructure').text();
                        } else if (ha$xml.find('trigger').length > 0) {
                            body = ha$xml.find('trigger').text();
                            pattern = 'trigger';
                        } else {
                            body = ha$message.text();
                        }
                    } else {
                        body = "";
                    }
                    var delayed = ha$delay.length > 0,
                        fullname = this.get('fullname'),
                        is_groupchat = type === 'groupchat';


                    if (is_groupchat) {
                        from = Strophe.getBareJidFromJid(ha$message.attr('from'));

                        if (!from) {
                            from = Strophe.getBareJidFromJid(ha$message.attr('from'));
                        } else {
                            if (from.indexOf(hanaCore.sky_hanaprefix) !== -1) {
                                var jidSend = hanaCore.sky_ai_name;
                                fullname = jidSend;
                                from = jidSend;
                            }
                            ha_.each(hanaCore.all_agent.users, function (userx) {
                                if (userx.username == Strophe.getNodeFromJid(ha$message.attr('from'))) {
                                    from = userx.name;
                                    fullname = userx.name;
                                }
                            });
                        }
                    } else {
                        from = Strophe.getBareJidFromJid(ha$message.attr('from'));
                    }
                    if (ha_.isEmpty(fullname)) {
                        fullname = from;
                    }
                    if (delayed) {
                        stamp = ha$delay.attr('stamp');
                        time = stamp;
                    } else {
                        // time = moment().format();
                    }
                    if ((is_groupchat && from === this.get('nick') || (is_groupchat && from === hanaCore.bare_jid)) || (!is_groupchat && from === hanaCore.bare_jid)) {
                        sender = hanaCore.sky_myname;
                    } else {
                        sender = 'them';
                    }
                    var timeS = ha$message.attr('timesend');
                    var timesend = ha$message.attr('timesend');
                    var timereceive = timereceivestore;

                    var timesendlocal = Number(Date.now()) + (Number(timereceive) - Number(timesend));
                    if (!timeS) {

                    } else {
                        var date = new Date(Number(timeS));
                        time = date;
                    }

                    return {
                        'type': type,
                        'chat_state': '',
                        'delayed': delayed,
                        'fullname': fullname,
                        'message': body || undefined,
                        'pattern': pattern,
                        'pathurl': pathurl,
                        'filename': filename,
                        'msgid': ha$message.attr('id'),
                        'sender': sender,
                        'timesend': timesend,
                        'timesendorg': timesend,
                        'sort': sort,
                        'attachment': attachment,
                        'sendtype': 'storemsg',
                        'time': time
                    };
                },
            });
            hanaCore.ChatCollapseView = Backbone.View.extend({
                tagName: 'div',
                id: 'hana-popchat',
                events: {
                    'click .hana-coll-open': 'openChat',
                },

                initialize: function () {

                },
                render: function () {
                    hanaCore.log('RENDER COLLAPSE ' + hanaCore.collapse_template);
                    if (hanaCore.collapse_template === 1) {
                        hanaCore.log('RENDER COLLAPSE 12 ' + hanaCore.collapse_template);
                        hanaCore.frameCollapseDiv.css("cssText", hanaCore.styleCollapse);
                    }
                    var ha$template = this.$el.html(tpl_bodycollapse({
                        heading: 'CHAO',
                        hanaimage: hanaCore.sky_icon_main,
                        collapse_template: hanaCore.collapse_template,
                        background: hanaCore.bgColor
                    }));
                    hanaCore.frameCollapseBody.append(ha$template);

                },
                openChat: function () {
                    hanaCore.actionCollapse('show');

                },
                insertIntoDOM: function () {

                }

            });
            hanaCore.ChatHeaderView = Backbone.View.extend({
                tagName: 'div',
                className: 'hana-chat-profile',
                id: 'hana-chat-profile',
                events: {
                    'click .hana-collapse-down': 'close',

                },

                initialize: function () {

                },
                render: function () {
                    //console_bk.log('RENDER HEADER');
                    var ha$template = this.$el.html(tpl_chatroomheader({
                        heading: 'CHAO',
                        show_form: hanaCore.showForm,
                        hananame: hanaCore.sky_ai_name,
                        hanadesc: hanaCore.sky_ai_desc ? hanaCore.sky_ai_desc: 'Support Specialist',
                        background: hanaCore.bgColor,
                        hanaimage: hanaCore.sky_icon_main,
                        domain: hanaCore.resource_popup
                    }));

                    return ha$template;
                },
                appendToBody: function () {
                    hanaCore.frameBody.find('.hana-chat-container').prepend(this.$el);
                },
                close: function () {
                    //console_bk.log('Header hidehide');
                    hanaCore.actionCollapse('hide');

                },
                insertIntoDOM: function () {

                }

            });
            hanaCore.ChatFooterView = Backbone.View.extend({
                tagName: 'div',
                className: 'hana-conversation-footer',
                id: 'hana-conversation-footer',
                events: {
                    'keyup textarea.mideas-input-message': 'keyPressed',
                    'click .hana-composer-send-button': 'keyClickRaw',
                    'click .hana-composer-upload-button': 'sendMessageImage',
                    'click .hana-button-start': 'startChat',
                    'click .hana-website': 'goToHana'


                },
                initialize: function () {
                },
                insertIntoDOM: function () {

                },
                render: function () {
                    //console_bk.log('RENDER FOOTER ' + hanaCore.powerby_hide);
                    if (window.is_ie) {
                        var ha$template = this.$el.html(tpl_chatroomfooter(
                            ha_.extend(
                                this.model.toJSON(), {
                                    heading: 'CHAO',
                                    ie: 'true',
                                    background: hanaCore.bgColor,
                                    show_form: hanaCore.showForm,
                                    powerby: hanaCore.powerby_hide

                                })));
                    } else {
                        var ha$template = this.$el.html(tpl_chatroomfooter(
                            ha_.extend(
                                this.model.toJSON(), {
                                    heading: 'CHAO',
                                    ie: 'false',
                                    background: hanaCore.bgColor,
                                    show_form: hanaCore.showForm,
                                    powerby: hanaCore.powerby_hide,
                                    collapse_template: hanaCore.collapse_template
                                })
                        ));
                    }
                    return ha$template;

                },
                appendToBody: function () {
                    hanaCore.frameBody.find('.hana-conversation').append(this.$el);
                },
                keyPressed: function (ev) {

                    var modelRoom = hanaCore.chatBoxModel;
                    if (ev) {
                        var ha$textarea = ha$(ev.target),
                            message;
                        if (ev.keyCode === KEY.ENTER) {
                            ev.preventDefault();
                            hanaCore.chatRoomView.$el.css({ "top": "75px", "bottom": "85px" });
                            this.$el.find('pre').text('');
                            message = ha$textarea.val().trim();
                            if (hanaCore.checkMobile() == 'mobile') {
                                ha$textarea.val('');
                            } else {
                                ha$textarea.val('').focus();
                            }
                            hanaCore.iqBroadCastAction('untyping');
                            if (message !== '') {
                                if (message.length > 4000) {
                                    alert('Bạn chỉ được nhập tối đa 4000 ký tự ');
                                    return;
                                }
                                if (modelRoom.get('type') === 'chatroom') {
                                    this.onChatRoomMessageSubmitted(message);
                                }
                            }

                        } else { // chat state data is currently only for single user chat
                            message = ha$textarea.val();
                            if (message === '') {
                                hanaCore.iqBroadCastAction('untyping');
                                hanaCore.chatRoomView.$el.css({ "top": "75px", "bottom": "85px" });
                                return;
                            }
                            hanaCore.iqBroadCastAction('typing', message);
                            this.$el.find('pre').text(message + '</br>');
                            var height = 30 + parseInt(this.$el.find('.hana-footerarea').height());
                            hanaCore.chatRoomView.$el.css({ "top": "75px", "bottom": height + "px" });

                        }
                    } else {
                    }
                },
                goToHana: function () {
                    var win = window.open('https://hana.ai', '_blank');
                    if (win) {
                        win.focus();
                    } else {
                        //Browser has blocked it
                    }
                },
                startChat: function () {
                    var ha$chatBody = hanaCore.chatRoomView.$el;
                    var ha$name = ha$chatBody.find('#name'),
                        name = ha$name.val().trim();
                    var ha$email = ha$chatBody.find('#email'),
                        email = ha$email.val().trim();
                    var ha$phone = ha$chatBody.find('#phone'),
                        phone = ha$phone.val().trim();
                    var ha$location = ha$chatBody.find('#location'),
                        location = ha$location.val().trim();
                    if (name == '') {
                        ha$name.addClass('hana-input-boder-error');
                        return;
                    }
                    if (email == '') {
                        ha$email.addClass('hana-input-boder-error');
                        return;
                    } else {
                        if (!hanaCore.validEmail(email)) {
                            ha$email.addClass('hana-input-boder-error');
                            return;
                        }
                    }
                    if (phone == '') {
                        ha$phone.addClass('hana-input-boder-error');
                        return;
                    }
                    if (hanaCore.require_terms) {
                        var ha$checbox = ha$chatBody.find('#hana-accept-term'),
                            checbox = ha$checbox.is(':checked');
                        if (!checbox) {
                            ha$checbox.addClass('hana-input-boder-error');
                            return;
                        }

                    }
                    hanaCore.saveCustomer(name, email, phone, location, function () { });
                    hanaCore.mideas_require_infomation = false;
                    hanaCore.showForm = false;
                    hanaCore.chatFooterView.render();
                    hanaCore.chatFooterView.appendToBody();
                    hanaCore.chatRoomView.render();
                    hanaCore.chatRoomView.appendToBody();
                    hanaCore.chatHeaderView.render();
                    hanaCore.chatHeaderView.appendToBody();

                },

                keyClickRaw: function (ev) {
                    /* Event handler for when a key is pressed in a chat box textarea.
                     */
                    // var ha$textarea = ha$('.mideas-input-message'),
                    var ha$textarea = this.$el.find('.mideas-input-message'),
                        message = ha$textarea.html();
                    if (message == '') {
                        message = ha$textarea.val();
                    }

                    /*if (hanaCore.checkMobile() == 'mobile') {
                        ha$textarea.val('');
                    } else {
                        ha$textarea.val('').focus();
                    }*/
                    if (message !== '') {
                        ha$textarea.val('');
                        this.$el.find('pre').text('');
                        hanaCore.chatRoomView.$el.css({ "top": "75px", "bottom": "85px" });
                        this.onChatRoomMessageSubmitted(message);
                        hanaCore.emit('messageSend', message);
                    }


                },
                sendMessageImage: function (ev) {
                    ev.preventDefault();
                    var that = this;
                    var ha$inputfile = this.$el.find('#fileuploadimage');
                    ha$inputfile.click();
                    ha$inputfile.bind("change", function () {
                        // alert('changed!');
                        // append loading here
                        if (!this.value) {
                            return;
                        }
                        if (this.value == '') {
                            return;
                        }

                        var formData;
                        if (window.is_ie) {
                            formData = new ieFormData();
                        } else {
                            formData = new FormData();
                        }
                        //kiem tra file do la hinh hay file
                        // console_bk.log('valueê ' + this.value);
                        var ext = this.value.match(/\.([^\.]+)$/)[1],
                            typefile = 'file';
                        ext = ext.toLowerCase();
                        switch (ext) {
                            case 'png':
                            case 'jpg':
                            case 'gif':
                            case 'jpeg':
                                // alert('allowed');
                                typefile = 'image';
                                break;
                            default:
                                alert('Chỉ gửi các file có định dạng png,jpg,gif,jpeg');
                                // ha$jqueryTemp.remove();
                                ha$inputfile.val("");
                                return;
                        }
                        // var filesize = ha$('#fileuploadimage-' + imageappend)[0].files[0].size;
                        var filesize = ha$inputfile[0].files[0].size;
                        if (filesize >= 5 * 1024 * 1024) {
                            alert("Dung lương file không được quá 5M");
                            ha$inputfile.val("");
                            return;
                        }

                        formData.append('file', ha$inputfile[0].files[0]);
                        formData.append('msisdn', Strophe.getNodeFromJid(hanaCore.connection.jid));
                        formData.append('token', '30bf9d0f6bd0625ab15d93c605ad31a4');
                        formData.append('type', typefile);
                        formData.append('listUser', Strophe.getNodeFromJid(hanaCore.connection.jid));
                        ha$inputfile.unbind("change");
                        ha$inputfile.val("");
                        var url = hanaCore.hana_upload_api;
                        if (window.is_ie) {
                            ha$.ajax({
                                url: url,
                                type: 'POST',
                                data: formData,
                                processData: formData.processData,
                                contentType: formData.contentType,
                                cache: false,
                                success: function (data) {
                                    try {
                                        var image = [];
                                        var firstImage;
                                        ha_.each(data, function (da) {
                                            var imagelink = hanaCore.hana_nginx_domain + da;
                                            firstImage = imagelink;
                                            image.push(imagelink);
                                        });
                                        that.sendChatRoomImageMessage(image);
                                    } catch (e) {

                                    }
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                    alert("Lỗi xảy ra vui lòng kiểm tra lại kết nối");
                                }
                            });
                        } else {
                            ha$.ajax({
                                url: url,
                                type: 'POST',
                                data: formData,
                                processData: false, // tell jQuery not to process the data
                                contentType: false, // tell jQuery not to set contentType
                                success: function (data) {
                                    try {
                                        var image = [];
                                        var firstImage;
                                        ha_.each(data, function (da) {
                                            var imagelink = hanaCore.hana_nginx_domain + da;
                                            firstImage = imagelink;
                                            image.push(imagelink);
                                        });
                                        that.sendChatRoomImageMessage(image);
                                    } catch (e) {

                                    }
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                    alert("Lỗi xảy ra vui lòng kiểm tra lại kết nối");
                                }
                            });
                        }

                    });
                },
                sendChatRoomImageMessage: function (image) {
                    var msgid = hanaCore.connection.getUniqueId();
                    var timesend = Number(Date.now());
                    var modelRoom = hanaCore.chatBoxModel;
                    var room = modelRoom.get('jid') + hanaCore.hana_room;
                    var textjson = { "type": "images", "body": "", "img_url": image, "msg_id": msgid };
                    var base64Text = hanaCore.base64Encode(JSON.stringify(textjson));
                    var attachment = { "attachment": { "payload": { "url": image }, "type": "image" } };
                    hanaCore.chatBoxModel.messages.create({
                        fullname: this.model.get('nick'),
                        sender: hanaCore.sky_myname,
                        membersend: Strophe.getNodeFromJid(hanaCore.connection.jid),
                        time: timesend,
                        message: base64Text,
                        pattern: 'text',
                        to: room,
                        timesend: timesend,
                        attachment: hanaCore.base64Encode(JSON.stringify(attachment)),
                        msg_state: hanaCore.STATEMESSAGE.NOTSENT,
                        sendtype: 'presskey',
                        msgid: msgid,
                        id: msgid
                    });

                    var msg = ha$msg({
                        to: room,
                        from: hanaCore.connection.jid,
                        type: 'groupchat',
                        id: msgid
                    }).c("body").t(base64Text);
                    hanaCore.connection.send(msg);
                },
                onChatRoomMessageSubmitted: function (text, payload) {
                    this.sendChatRoomMessage(text, payload);
                },
                countMessageSend: function () {
                    if (this.model.get('is_block')) {
                        return true;
                    } else {
                        var countInc = this.model.get('count_block') + 1;
                        this.model.set('count_block', countInc);
                        return false;
                    }
                },
                sendChatRoomMessage: function (text, payload) {
                    var base64Text;
                    var msgid = hanaCore.connection.getUniqueId();
                    if (!window.is_ie) {
                        hanaCore.chatRoomView.messageSendFail[msgid] = msgid;
                    }
                    var modelRoom = hanaCore.chatBoxModel;
                    //console_bk.log('ROOM ' + modelRoom.get('jid') + ' SENDER ' + hanaCore.connection.jid);
                    if (!this.countMessageSend()) {
                        if (payload) {
                            var textjson = { "type": "postback", "body": JSON.parse(hanaCore.base64Decode(payload)), "msg_id": msgid };
                            base64Text = hanaCore.base64Encode(hanaCore.regexReplaceEnter(JSON.stringify(textjson)));
                        } else {
                            var textjson = { "type": "text", "body": text, "msg_id": msgid };
                            base64Text = hanaCore.base64Encode(hanaCore.regexReplaceEnter(JSON.stringify(textjson)));
                        }
                        // console_bk.log('send to server ' + base64Text + ' payload ' + payload);
                        var msg = ha$msg({
                            to: modelRoom.get('jid') + hanaCore.hana_room,
                            from: hanaCore.connection.jid,
                            type: 'groupchat',
                            id: msgid
                        }).c("body").t(base64Text);

                        hanaCore.chatBoxModel.messages.create({
                            fullname: '',
                            sender: hanaCore.sky_myname,
                            time: '',
                            message: base64Text,
                            pattern: 'text',
                            timesend: Date.now(),
                            sendtype: 'presskey',
                            msgid: msgid,
                            id: msgid
                        });
                        hanaCore.connection.send(msg);
                    } else {

                    }

                },

            });
            hanaCore.ChatRoomView = Backbone.View.extend({
                tagName: 'div',
                className: 'hana-chat-body',
                id: 'hana-chat-body',
                events: {
                    'click .mideas-btn-like': 'likeConversation',
                    'click .mideas-btn-dislike': 'dislikeConversation',
                    'click .hana-quickreply-action': 'sendQuickReply',
                    'click .hana-form-input': 'typePing'
                },

                initialize: function () {
                    var that = this;
                    this.model.messages.on('add', this.onMessageAdded, this);
                    this.messageSendFail = {};
                    setInterval(function () { that.executeFailMessage(); }, 5 * 1000);
                    this.listenMessage();
                    this.$el.css({ "top": "75px", "bottom": "85px" });

                },
                insertIntoDOM: function () {

                },
                listenMessage: function () {
                    hanaCore.connection.addHandler(this.handleMUCStanzaRaw.bind(this), null, 'message', null);
                    // hanaCore.on('messagereceive', this.handleMUCStanzaRaw);
                },
                render: function () {
                    // console_bk.log('RENDER BODYCHAT SHOWFORM ' + hanaCore.showForm);
                    var ha$template;
                    if (hanaCore.showForm) {
                        // console_bk.log('RENDER BODYCHAT SHOWFORM 1 ' + hanaCore.showForm);
                        ha$template = this.$el.html(tpl_chatformbody(
                            ha_.extend(
                                this.model.toJSON(), {
                                    heading: 'CHAO',
                                    show_form: hanaCore.showForm
                                })));
                        if (hanaCore.require_terms) {
                            ha$template.find('.hana-custome-view').prepend(ha$(hanaCore.terms_html));
                        }
                        ha$template.find('input').placeholder();
                    } else {
                        // console_bk.log('RENDER BODYCHAT SHOWFORM 1 ' + hanaCore.showForm);
                        ha$template = this.$el.html(tpl_chatroombody(
                            ha_.extend(
                                this.model.toJSON(), {
                                    heading: 'CHAO',
                                    show_form: hanaCore.showForm
                                })));
                        this.fetchMessagesOffline(0, 0, 20, 0);
                    }
                    return ha$template;


                },
                appendToBody: function () {
                    hanaCore.frameBody.find('.hana-chat-container').prepend(this.$el);
                    this.ha$content = this.$el.find('.mideas-message-chat');
                    this.ha$contentDiv = this.$el.find('.mideas-wrapper-popup-body');
                },
                typePing: function (ev) {
                    var ha$tagert = ha$(ev.target);
                    ha$tagert.removeClass('hana-input-boder-error');
                },
                sendWelcomeToServer: function () {
                    hanaCore.getBlock(this.getBlockCB);
                },
                getBlockCB: function (data) {
                    //console_bk.log('JSONMODEL ' + JSON.stringify(hanaCore.chatBoxModel));
                    if (data) {
                        var json = data;
                        if (json.code == 200) {
                            var data = json.data;
                            if (data.length > 0) {
                                var block = data[0].blocks;
                                var timesend = data.timesend;
                                var infoUser = ha_.find(block, function (obj) { return obj.type == 'welcome' });
                                if (infoUser) {
                                    hanaCore.chatBoxModel.createMessageWelcome(infoUser.content_validate, timesend);
                                }
                            }
                        } else {

                        }

                    }
                },
                fetchMessagesOffline: function (beginTime, endTime, sizeMsg, sort) {
                    var that = this;
                    var from = hanaCore.connection.jid;
                    from = Strophe.getNodeFromJid(from);
                    var to = this.model.get('jid');
                    to = to + hanaCore.hana_room;
                    var infoUser = JSON.stringify({ "room_name": to, "unix_start": beginTime, "unix_end": endTime, "size_msg": sizeMsg, "sort": sort });
                    hanaCore.log('SENDEVENTTO SERVER FROM ' + from);
                    hanaCore.getStoreMessage(to, beginTime, endTime, sizeMsg, sort, function (data) {
                        if (data) {
                            var json = data;
                            var msgJson = json.msg;
                            if (hanaCore.newsession == 'true' || msgJson.length == 0) {
                                that.sendWelcomeToServer('first');
                            }
                            ha_.each(msgJson, function (msgJ) {
                                var msgPacket = msgJ.rawmsg;
                                that.onChatRoomMessageOffline(msgPacket, json.timereceive, sort);
                            });
                        } else {

                        }
                    });
                    return this;
                },

                onChatRoomMessageOffline: function (message, timereceive, sort) {
                    hanaCore.log("[ON MESSAGE GROUP ]" + message);
                    var ha$message = ha$(message),
                        ha$forwarded = ha$message.find('forwarded'),
                        ha$delay;
                    if (ha$forwarded.length) {
                        var jid = ha$message.attr('to');
                        var myJid = hanaCore.connection.jid;
                        ha$message = ha$forwarded.children('message');
                        ha$delay = ha$forwarded.children('delay');
                        //check if resource is not same
                        hanaCore.log("Check if same id when continue: " + jid + " My: " + myJid);
                        if (jid != myJid) {
                            return true;
                        }
                        //TODO
                    }
                    var jid = ha$message.attr('from'),
                        msgid = ha$message.attr('id'),
                        sender = ha$message.attr('member');

                    if (sender === '') {
                        return true;
                    }
                    this.model.createMessageStore(message, ha$delay, message, timereceive, sort);
                    if (sender !== this.model.get('nick')) {
                        hanaCore.emit('message', message);
                    }
                    return true;
                },
                sendQuickReply: function (ev) {
                    var ha$quick = ha$(ev.target);
                    var data = ha$quick.data('payload');
                    try {
                        var jsonPay = JSON.parse(hanaCore.base64Decode(data));
                        hanaCore.chatFooterView.onChatRoomMessageSubmitted(data, data);
                    } catch (e){
                        // console_bk.log('catchthismodel ');
                        hanaCore.chatFooterView.onChatRoomMessageSubmitted(data);
                    }
                    //console_bk.log(data);
                },
                handleMUCStanzaRaw: function (stanza) {
                    var xmlText;
                    if (stanza.xml) {
                        //console_bk.log('STRINGSTANXA ' + stanza.xml);
                        var ts = ha$(stanza.xml);
                        var se = new XMLSerializer();
                        // xmlText = se.serializeToString(ts[0]),
                        xmlText = stanza.xml;

                    } else {
                        xmlText = new XMLSerializer().serializeToString(stanza);
                        //console_bk.log('STRINGSTANXA11 ' + xmlText);
                    }
                    hanaCore.log('STRINGSTANXA11xmlText ' + xmlText);
                    if (stanza.nodeName === "message") {
                        var xml = ha$.parseXML(xmlText);
                        var ha$xml = ha$(xml);
                        var ha$message = ha$(xmlText),
                            ha$delay = ha$message.find('delay'),
                            type = ha$message.attr('type'),
                            ha$forwarded = ha$message.find('forwarded');
                        if (ha$delay.length) {
                            if (ha$message.find('body').length > 0) {
                                if (!ha$forwarded.length) {
                                    hanaCore.connection.send(
                                        ha$msg({ to: stanza.getAttribute('from'), type: type, id: 'popupbody---' + Math.random().toString(36).substr(2, 10) })
                                            .c("x", {
                                                xmlns: "jabber:x:event"
                                            })
                                            .c("delivered").up()
                                            .c("msgid").t(ha$message.attr('id'))
                                    );
                                }

                            }
                            return true;
                        }
                        var is_event_msg = ha$message.find('[xmlns="jabber:x:event"]').length > 0;
                        if (is_event_msg) {
                            var hasSent = ha$message.find('sent').length > 0;
                            var delivered = ha$message.find('delivered').length > 0;
                            var displayed = ha$message.find('displayed').length > 0;
                            var blocked = ha$message.find('blocked').length > 0;
                            if (hasSent) {
                                var msgIdSent = ha$message.children('x').children('msgid').text();
                                var msgIdSent = ha$xml.find('msgid').text();
                                hanaCore.log("has Send " + msgIdSent);
                                if (msgIdSent.length > 0) {
                                    var modelMsg = this.model.messages.get(msgIdSent);
                                    if (this.model.messages.get(msgIdSent)) {
                                        this.model.messages.get(msgIdSent).set({ 'timesendorg': ha$message.attr('timereceive') });
                                    }
                                    if (modelMsg) {
                                        modelMsg.set("msg_state", hanaCore.STATEMESSAGE.HASSENT);
                                        this.ha$content.find('.mideas-chat-message[data-msgid="' + msgIdSent + '"]').find('.mideas-chat-msg-content').removeClass("mideas-msg-send-fail");
                                        this.ha$content.find('.mideas-chat-message[data-msgid="' + msgIdSent + '"]').find('.loading-image').remove();
                                        delete this.messageSendFail[msgIdSent];
                                    }
                                }
                            }
                            if (delivered) {
                                var msgIdSent = ha$message.children('x').children('msgid').text();
                                if (msgIdSent.length > 0) {
                                    var modelMsg = this.model.messages.get(msgIdSent);
                                    if (modelMsg) {
                                        modelMsg.set("msg_state", hanaCore.STATEMESSAGE.HASRECEIVED);
                                    }

                                }
                            }
                            if (displayed) {
                                var msgIdSent = ha$message.children('x').children('msgid').text();
                                hanaCore.log("has displayed " + msgIdSent);
                                hanaCore.log("has displayed");
                                var modelMsg = this.model.messages.get(msgIdSent);
                                if (modelMsg) {
                                    modelMsg.set("msg_state", hanaCore.STATEMESSAGE.HASREAD);
                                }
                            }
                            if (blocked) {
                                this.model.set('is_block', true);
                            }

                        } else {

                        }
                    }

                    var xmlns, xquery, i;
                    var from = ha$message.attr('from');
                    var subtype = ha$message.attr('subtype');
                    if (subtype == 'kickweb') {
                        var jid = from;
                        hanaCore.log("[HUYNHDC create room aa] " + jid);
                        ha_.each(hanaCore.all_agent.users, function (userx) {
                            if (userx.username == ha$message.attr('member')) {
                                from = userx.name;
                            }
                        });
                        return true;
                    }

                    if (stanza.nodeName === "message") {

                        if (ha$message.find('attachment').length > 0) {
                            this.renderTypingTempate('typing');
                            var thats = this;
                            setTimeout(function () {
                                ha_.compose(thats.onChatRoomMessage.bind(thats), thats.showStatusMessages.bind(thats))(xmlText);
                            }, 2000 + hanaCore.timeOut_read);
                        } else if (ha$message.find('frontendstructure').length > 0) {
                            this.renderTypingTempate('typing');
                            var thats = this;
                            setTimeout(function () {
                                ha_.compose(thats.onChatRoomMessage.bind(thats), thats.showStatusMessages.bind(thats))(xmlText);
                            }, 2000 + hanaCore.timeOut_read);
                        } else {
                            ha_.compose(this.onChatRoomMessage.bind(this), this.showStatusMessages.bind(this))(xmlText);
                        }
                    } else if (stanza.nodeName === "presence") {
                    }
                    return true;
                },
                executeFailMessage: function () {
                    var that = this;
                    ha$.each(that.messageSendFail, function (index, value) {
                        that.ha$content.find('.mideas-chat-message[data-msgid="' + value + '"]').find('.mideas-chat-msg-content').addClass("mideas-msg-send-fail");
                        delete that.messageSendFail[value];
                    });
                },

                insertAgentJoin: function (attrs) {
                    var insert = this.ha$content.append;
                    ha_.compose(
                        this.scrollDownMessageHeight.bind(this),
                        function ($el) {
                            insert.call(this.ha$content, $el);
                            return $el;
                        }.bind(this)
                    )(this.renderAgentJoin(attrs));
                },
                renderAgentJoin: function (attrs) {
                    hanaCore.log('AGENT NAME ' + attrs.fullname);
                    var template = hanaCore.templates.join_hana;
                    this.$el.find(".mideas-name").html(attrs.fullname);
                    return ha$(template(
                        ha_.extend(this.getExtraMessageTemplateAttributes(attrs), {
                            'extra_classes': "",
                            'isodate': attrs.isodate,
                            'fullname': attrs.fullname
                        })
                    ))
                },
                onChatRoomMessage: function (message) {
                    // hanaCore.log("[ON MESSAGE GROUP ]" + message);
                    // this.scrollDown();
                    var xml = ha$.parseXML(message);
                    var ha$xml = ha$(xml);
                    var ha$message = ha$(message),
                        ha$forwarded = ha$xml.find('forwarded'),
                        ha$delay;
                    if (ha$forwarded.length) {
                        var jid = ha$message.attr('to');
                        var myJid = hanaCore.connection.jid;
                        ha$message = ha$forwarded.children('message');
                        ha$delay = ha$forwarded.children('delay');
                        //check if resource is not same
                        hanaCore.log("Check if same id when continue: " + jid + " My: " + myJid);
                        if (jid != myJid) {
                            return true;
                        }
                        //TODO
                    }
                    var jid = ha$message.attr('from'),
                        msgid = ha$message.attr('id'),
                        sender = ha$message.attr('member'),
                        type = ha$message.attr('type');

                    if (sender === '') {
                        return true;
                    }

                    if ((ha$xml.find('body').length > 0) || (ha$xml.find('attachment').length > 0) || (ha$xml.find('frontendstructure').length > 0) || (ha$xml.find('broadcast').length > 0)) {
                        this.model.createMessage(ha$message, ha$delay, message);
                        if (!ha$forwarded.length) {
                            if (sender !== this.model.get('nick')) {
                                // We only emit an event if it's not our own message
                                hanaCore.emit('message', message);
                            }
                            hanaCore.connection.send(
                                ha$msg({ to: jid, type: type, id: 'popupbody---' + Math.random().toString(36).substr(2, 10) })
                                    .c("x", {
                                        xmlns: "jabber:x:event"
                                    })
                                    .c("delivered").up()
                                    .c("msgid").t(ha$message.attr('id'))
                            );
                        }
                    }
                    if (ha$xml.find('delivered').length > 0) {
                        if (!ha$forwarded.length) {

                        }
                    }
                    return true;
                },
                showStatusMessages: function (el, is_self) {
                    var $el = ha$(el);
                    return el;
                },
                onMessageAdded: function (message) {
                    if (message.get('type') === 'error') {
                        // that.handleErrorMessage(message);
                    } else {
                        this.handleTextMessage(message);

                        hanaCore.chatRoomNotifyView.hideOrShow(message);

                    }
                },

                handleTextMessage: function (message) {
                    if (message.attributes.sendtype == 'presskey') {
                        this.renderTypingTempate('untyping');
                        this.removeQuickReplies();
                        var jsonMsg = hanaCore.base64Decode(message.attributes.message);
                        var messageJson = JSON.parse(jsonMsg);
                        hanaCore.log('handleTextMessageJSON ' + JSON.stringify(messageJson));
                        if (messageJson.type === 'images') {
                            message.attributes.pattern = 'image';
                        } else if (messageJson.type === 'postback') {
                            message.attributes.pattern = 'postback';
                        } else {
                            message.attributes.pattern = 'text';
                        }
                        this.showMessageSend(ha_.clone(message.attributes));
                    } else if (message.attributes.sendtype == 'storemsg') {
                        this.showMessageReceiveStore(ha_.clone(message.attributes));
                    } else {
                        this.removeQuickReplies();
                        this.showMessageReceive(ha_.clone(message.attributes));
                    }

                },
                showMessageReceive: function (attrs) {
                    var that = this;
                    var msg_dates, idx,
                        ha$first_msg = this.ha$content.children('.mideas-chat-message:first'), //get first message
                        first_msg_date = ha$first_msg.data('isodate'), //first message date
                        timeMsg = Number(attrs.timesend),
                        last_msg_date = this.ha$content.children('.mideas-chat-message:last').data('isodate'); //last message date

                    if (!first_msg_date) { //if first message
                        this.insertMessageReceive(attrs);
                        return;
                    }
                    if (timeMsg >= last_msg_date) { //insert last message date
                        this.insertMessageReceive(attrs);
                        return;
                    }
                    if (timeMsg <= first_msg_date) { //
                        this.insertMessageReceive(attrs, 'prepend');
                        return;
                    }
                    this.insertMessageReceive(attrs);
                    return;
                },
                showMessageReceiveStore: function (attrs) {
                    var that = this;
                    var hasMsg = this.ha$content.find('.mideas-chat-message[data-msgid="' + attrs.msgid + '"]');
                    if (hasMsg.length > 0) {
                        return;
                    }
                    var msg_dates, idx,
                        ha$first_msg = this.ha$content.children('.mideas-chat-message:first'), //get first message
                        first_msg_date = ha$first_msg.data('isodate'), //first message date
                        timeMsg = Number(attrs.timesendorg),
                        // current_msg_date = Date.now(),             //current date
                        last_msg_date = this.ha$content.children('.mideas-chat-message:last').data('isodate'); //last message date

                    if (!first_msg_date) { //if first message
                        this.insertMessageReceiveStore(attrs);
                        return;
                    }
                    if (timeMsg >= last_msg_date) { //insert last message date
                        this.insertMessageReceiveStore(attrs);
                        return;
                    }
                    if (timeMsg <= first_msg_date) { //
                        this.insertMessageReceiveStore(attrs, 'prepend');
                        return;
                    }
                    msg_dates = ha_.map(this.ha$content.children('.mideas-chat-message'), function (el) {
                        return ha$(el).data('isodate');
                    });
                    msg_dates.push(timeMsg);
                    msg_dates.sort();
                    idx = msg_dates.indexOf(timeMsg) - 1;
                    ha_.compose(
                        this.scrollDownMessageHeight.bind(this),
                        function ($el) {
                            $el.insertAfter(that.ha$content.find('.mideas-chat-message[data-isodate="' + msg_dates[idx] + '"]'));
                            return $el;
                        }.bind(this)
                    )(this.renderMessageReceiveStore(attrs));
                },
                renderMessageReceiveStore: function (attrs) {

                    var text = attrs.message,
                        // match = text.match(/^\/(.*?)(?: (.*))?ha$/),
                        fullname = this.model.get('fullname') || attrs.fullname,
                        extra_classes = attrs.delayed && 'delayed' || '',
                        template, username;

                    var messageJson = JSON.parse(hanaCore.base64Decode(attrs.message));
                    if (messageJson.type === 'images') {
                        attrs.pattern = 'image';
                    } else if (messageJson.type === 'attachment') {
                        attrs.pattern = 'attachment';
                    } else if (messageJson.type === 'postback') {
                        attrs.pattern = 'postback';
                    } else {
                        attrs.patternn = 'text';
                    }
                    hanaCore.log('MESSAGE JSON ' + JSON.stringify(messageJson));
                    if (attrs.pattern == 'text' || attrs.pattern == 'postback') {
                        if (attrs.sender === hanaCore.sky_myname) {
                            text = text.replace(/^\/me/, '');
                            template = tpl_msgtextme;
                            username = attrs.sender;
                        } else {
                            template = tpl_msgtexttheir;
                            username = attrs.sender === hanaCore.sky_myname && __(hanaCore.sky_myname) || fullname;
                        }
                    } else if (attrs.pattern == 'image') {
                        if (attrs.sender === hanaCore.sky_myname) {
                            template = tpl_msgimageme;
                            username = attrs.sender;
                        } else {
                            template = tpl_msgimagetheir;
                            username = attrs.sender === hanaCore.sky_myname && __(hanaCore.sky_myname) || fullname;
                        }
                    } else if (attrs.pattern == 'file') {
                        if (attrs.sender === hanaCore.sky_myname) {
                            template = tpl_msgfileme;
                            username = attrs.sender;
                        } else {
                            template = tpl_msgfiletheir;
                            username = attrs.sender === hanaCore.sky_myname && __(hanaCore.sky_myname) || fullname;
                        }
                    } else if (attrs.pattern == 'attachment') {
                        if (attrs.sender === hanaCore.sky_myname) {
                            template = tpl_msgfileme;
                            username = attrs.sender;
                        } else {
                            template = tpl_msgfiletheir;
                            username = attrs.sender === hanaCore.sky_myname && __(hanaCore.sky_myname) || fullname;
                        }
                    }
                    this.ha$content.find('div.chat-event').remove();

                    if (attrs.pattern == 'text' ||  attrs.pattern == 'postback') {
                        // var textjson = JSON.parse(hanaCore.base64Decode(text));
                        var textjson, bodytext;
                        if (attrs.pattern == 'text') {
                            textjson = JSON.parse(hanaCore.base64Decode(text));
                            bodytext = textjson.body;
                            attrs.message = textjson.body;
                        } else {
                            textjson = JSON.parse(hanaCore.base64Decode(text));
                            bodytext = textjson.body.title;
                        }
                        attrs.message = textjson.body;
                        if (!hanaCore.sky_first_msg_store) {
                            hanaCore.log('MESSAGETEXT  ' + attrs.message);
                            hanaCore.sky_first_msg_store = true;
                            this.renderCollapseText(attrs);
                        }
                        // console_bk.log('BODY OFFLINE '+ bodytext + ' PAYLOAD ' + attrs.pattern);
                        var ha$temp =  ha$(template(
                            ha_.extend(this.getExtraMessageTemplateAttributes(attrs), {
                                'msgid': attrs.msgid,
                                'sender': attrs.sender,
                                'time': '',
                                'isodate': attrs.timesend,
                                'background': hanaCore.bgColorcontent,
                                'colorcontent': hanaCore.colorcontent,
                                'colorcontentcskh': hanaCore.bgColorcontentcskh,
                                'sky_icon_main': hanaCore.sky_icon_main,
                                'username': username,
                                'version': window.is_ie,
                                'message': this.texthtmlDecode(bodytext),
                                'suggestions': [],
                                'extra_classes': extra_classes
                            })
                        ));
                        ha$temp.find('.mideas-chat-msg-content').addHyperlinks().addEmoticons();
                        return ha$temp;
                    } else if (attrs.pattern == 'image') {
                        // var attachmentEle = JSON.parse(this.base64Decode(attrs.attachment));

                        return ha$(template(
                            ha_.extend(this.getExtraMessageTemplateAttributes(attrs), {
                                'msgid': attrs.msgid,
                                'sender': attrs.sender,
                                'time': '',
                                'isodate': attrs.timesend,
                                'sky_icon_main': hanaCore.sky_icon_main,
                                'timesendorg': attrs.timesend,
                                'username': username,
                                'message': '',
                                // 'pathurl': attachmentEle.attachment.payload.url,
                                // 'filename': attachmentEle.attachment.payload.name,
                                'type_image': 'single',
                                'pathurl': messageJson.img_url[0],
                                'extra_classes': extra_classes
                            })
                        ));
                    } else if (attrs.pattern == 'file') {
                        var attachmentEle = JSON.parse(this.base64Decode(attrs.attachment));
                        return ha$(template(
                            ha_.extend(this.getExtraMessageTemplateAttributes(attrs), {
                                'msgid': attrs.msgid,
                                'sender': attrs.sender,
                                'time': '',
                                'isodate': attrs.timesend,
                                'sky_icon_main': hanaCore.sky_icon_main,
                                'timesendorg': attrs.timesend,
                                'username': username,
                                'message': '',
                                'pathurl': attachmentEle.attachment.payload.url,
                                'filename': attachmentEle.attachment.payload.name,
                                'extra_classes': extra_classes
                            })
                        ));
                    } else if (attrs.pattern == 'attachment') {
                        var textjson = JSON.parse(hanaCore.base64Decode(text));
                        hanaCore.log('ATTACMENT FILE ' + JSON.stringify(textjson));
                        var url = JSON.parse(textjson.body).attachments[0].payload.url;
                        return ha$(template(
                            ha_.extend(this.getExtraMessageTemplateAttributes(attrs), {
                                'msgid': attrs.msgid,
                                'sender': attrs.sender,
                                'time': '',
                                'isodate': attrs.timesend,
                                'sky_icon_main': hanaCore.sky_icon_main,
                                'timesendorg': attrs.timesend,
                                'username': username,
                                'message': '',
                                'pathurl': url,
                                'filename': '',
                                'extra_classes': extra_classes
                            })
                        ));
                    } else {

                    }
                },
                insertMessageReceive: function (attrs, prepend) {
                    var that = this;
                    var insert = prepend ? this.ha$content.prepend : this.ha$content.append;
                    if (attrs.pattern == 'attachment') {
                        this.renderTypingTempate('untyping');
                        var insert = prepend ? this.ha$content.prepend : this.ha$content.append;
                        this.renderMessageAttachmentReceive(attrs, prepend);
                        // console_bk.log('Scrolll insertMessageReceive ');
                        setTimeout(function () {
                            that.scrollDownSlow();
                        }, 300);
                    } else if (attrs.pattern == 'suggestions') {
                        this.renderTypingTempate('untyping');
                        this.renderMessageSuggestionReceive(attrs);
                        setTimeout(function () {
                            that.scrollDownSlow();
                        }, 1000);
                    } else if (attrs.pattern == 'broadcast') {
                        this.renderMessageBroadcastReceive(attrs);
                        setTimeout(function () {
                            that.scrollDownSlow();
                        }, 1000);
                    } else {
                        this.renderTypingTempate('untyping');
                        ha_.compose(
                            this.scrollDownMessageHeight.bind(this),
                            function ($el) {
                                insert.call(this.ha$content, $el);
                                // if($el.find('.loading-image')) {
                                //     $el.find('.loading-image').remove();
                                // }

                                return $el;
                            }.bind(this)
                        )(this.renderMessageReceive(attrs));
                    }
                },
                renderMessageSuggestionReceive: function (attrs) {

                    /*var msg_time = moment(attrs.time) || moment,
                        text = attrs.message,
                        fullname = this.model.get('fullname') || attrs.fullname,
                        extra_classes = attrs.delayed && 'delayed' || '',
                        template, username, template_suggestion,
                        suggestion_value, template_sugg_return,
                        ha$suggestionItem = ha$('<ul class="mideas-slides slides"></ul>'),
                        ha$suggestionParent = ha$('<li class="mideas-flexslider"></li>');

                    if (attrs.sender === hanaCore.sky_myname) {
                        text = text.replace(/^\/me/, '');
                        template = hanaCore.templates.action_hana;
                        username = fullname;
                    } else {
                        template = hanaCore.templates.message_hana;
                        username = attrs.sender === hanaCore.sky_myname && __(hanaCore.sky_myname) || fullname;
                    }

                    this.ha$content.find('div.chat-event').remove();

                    // FIXME: leaky abstraction from MUC
                    if (this.is_chatroom && attrs.sender === 'them' && (new RegExp("\\b" + this.model.get('nick') + "\\b")).test(text)) {
                        extra_classes += ' mentioned';
                    }

                    var attachmentEle = this.base64Decode(attrs.suggestions);
                    var myObject = eval('(' + attachmentEle + ')');
                    var lengthSugg = myObject.length;
                    var that = this;
                    suggestion_value = [];
                    ha_.each(myObject, function (value, index) {
                        if (index % 3 == 0) {
                            suggestion_value = [],
                                template_suggestion = hanaCore.templates.suggestion;
                        }
                        suggestion_value.push(value.data);
                        if (index % 3 == 2 || index == (lengthSugg - 1)) {
                            var viewSugg = new hanaCore.SuggestionView({
                                model: ha_.extend(that.getExtraMessageTemplateAttributes(attrs), {
                                    'msgid': attrs.msgid,
                                    'sender': attrs.sender,
                                    'time': msg_time.format('hh:mm'),
                                    'isodate': attrs.timesend,
                                    'sky_icon_main': hanaCore.sky_icon_main,
                                    'username': username,
                                    'message': '',
                                    'that': that,
                                    'suggestions': suggestion_value,
                                    'extra_classes': extra_classes
                                })
                            });
                            ha$suggestionItem.append(viewSugg.render());
                        }

                    });
                    ha$suggestionParent.append(ha$suggestionItem);

                    //tim suggestion cuoi va xoa di truoc khi them suggestoin moi vao
                    var ha$lastSugg = this.ha$content.find('li:last');
                    if (ha$lastSugg.attr('class') == 'mideas-flexslider') {
                        ha$lastSugg.remove();
                    }
                    this.ha$content.append(ha$suggestionParent);

                    var countriesArray = ha$.map(myObject, function (value, key) {
                        return { value: value.data, data: key };
                    });

                    ha$('.mideas-input-message').autocomplete({
                        lookup: countriesArray,
                        lookupFilter: function (suggestion, originalQuery, queryLowerCase) {
                            var re = new RegExp('\\b' + ha$.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
                            return re.test(suggestion.value);
                        },
                        onSelect: function (suggestion) {
                            var sugVale = suggestion.value;

                            that.sendRawMessage(sugVale);
                            setTimeout(function () {
                                // that.ha$chatarea.html('');
                                that.ha$chatarea.val('').focus();
                            }, 100);

                        },
                        orientation: 'top',
                        lookupLimit: 5,
                        triggerSelectOnValidInput: false,

                    });

                    ha$suggestionParent.flexslider({
                        animation: ha$('.mideas-slides'),
                        animationLoop: false,
                        slideshow: false,
                        smoothHeight: true,
                    });*/

                },
                renderMessageBroadcastReceive: function (attrs) {
                    // // console_bk.log('ATTRS BROADCAST ' + JSON.stringify(attrs));
                    var bodyJson = JSON.parse(hanaCore.base64Decode(attrs.message));
                    if (bodyJson.type === 'typing') {
                        if (bodyJson.data.status === 'typing') {
                            this.renderTypingTempate('typing');
                        } else {
                            this.renderTypingTempate('untyping');
                        }
                    }
                },
                renderTypingTempate: function (status) {
                    if (status === 'untyping') {
                        if (!hanaCore.typingtempate) {
                            return false;
                        }
                        hanaCore.typingtempate.remove();
                        hanaCore.typingtempate = undefined;
                    } else {
                        var template = tpl_typing;
                        if (hanaCore.typingtempate) {
                            return false;
                        }
                        hanaCore.typingtempate = ha$(template(
                            ha_.extend({}, {})
                        ));
                        this.ha$content.append(hanaCore.typingtempate);
                    }
                    this.scrollDown();

                },
                renderMessageAttachmentReceive: function (attrs, prepend) {
                    var text = attrs.message,
                        fullname = this.model.get('fullname') || attrs.fullname,
                        extra_classes = attrs.delayed && 'delayed' || '',
                        template, username, template_suggestion,
                        ha$templateStructure;
                    //chọn template text, image, file
                    var stringRegex = hanaCore.regexReplaceEnter(hanaCore.base64Decode(attrs.attachment));
                    hanaCore.log('TRUOWC ' + hanaCore.base64Decode(attrs.attachment));
                    hanaCore.log('REGEXXXX ' + hanaCore.timeOut_read);
                    var attachmentContent = JSON.parse(stringRegex);

                    var that = this;
                    ha_.each(attachmentContent, function (attachment, index) {
                        var timeOutRead = hanaCore.timeOut_read * (1+index);
                        if (index == 0) { //first
                            if (attachment.message.text) {
                                if (attachment.message.quick_replies) { //quick replies
                                    if (attrs.sendtype == 'storemsg') {
                                        if (attachment.message.text != undefined && attachment.message.text != '') {
                                            attrs.message = attachment.message.text;
                                            attrs.payload = attachment.message.quick_replies;
                                            if (attrs.message instanceof Object) {
                                                if (attrs.message.type == 'template') {
                                                    attrs.payload = attachment.message.text.payload;
                                                    ha$templateStructure = that.renderMessageStructure(attrs);
                                                } else if (attrs.message.type == 'image') {
                                                    attrs.pathurl = attachment.message.text.payload.url;
                                                    attrs.filename = '';
                                                    attrs.payload = attachment.message.text.payload;
                                                    ha$templateStructure = that.renderMessageImage(attrs);
                                                }
                                            } else {
                                                ha$templateStructure = that.renderMessageText(attrs);
                                            }

                                            if (prepend == 'prepend') {
                                                that.ha$content.prepend(ha$templateStructure);
                                            } else {
                                                that.ha$content.append(ha$templateStructure);
                                            }
                                            if (ha$templateStructure.find('.hana-gallary')) {
                                                that.slickSlider(ha$templateStructure.find('.hana-gallary'));
                                            }
                                            //render quickreplies
                                            if (ha$('.mideas-message-chat').find('li').length == 1) {
                                                var ha$templateTempQuick = that.renderMessageQuickReplies(attrs);
                                                hanaCore.quickreplies.push(ha$templateTempQuick);
                                                ha$templateStructure = ha$templateTempQuick.insertAfter(ha$templateStructure);
                                                ha$templateTempQuick.find('.hana-quickreply').slick({
                                                    infinite: false,
                                                    speed: 300,
                                                    slidesToShow: 1,
                                                    variableWidth: true
                                                });
                                            }
                                        }
                                    } else {
                                        if (attachment.message.text != undefined && attachment.message.text != '') {
                                            attrs.message = attachment.message.text;
                                            attrs.payload = attachment.message.quick_replies;
                                            if (attrs.message instanceof Object) {
                                                if (attrs.message.type == 'template') {
                                                    attrs.payload = attachment.message.text.payload;
                                                    ha$templateStructure = that.renderMessageStructure(attrs);
                                                } else if (attrs.message.type == 'image') {
                                                    attrs.pathurl = attachment.message.text.payload.url;
                                                    attrs.filename = '';
                                                    attrs.payload = attachment.message.text.payload;
                                                    ha$templateStructure = that.renderMessageImage(attrs);
                                                }
                                            } else {
                                                ha$templateStructure = that.renderMessageText(attrs);
                                            }
                                            // ha$templateStructure = that.renderMessageText(attrs);
                                            // ha$templateStructure = ha$tempTextReplies;
                                            hanaCore.log('LENGTH WORDSS  ' + hanaCore.countWords(attrs.message));
                                            if (prepend == 'prepend') {
                                                that.ha$content.prepend(ha$templateStructure);
                                            } else {
                                                that.ha$content.append(ha$templateStructure);
                                            }
                                            if (ha$templateStructure.find('.hana-gallary')) {
                                                that.slickSlider(ha$templateStructure.find('.hana-gallary'));
                                            }
                                            var ha$templateTempQuick = that.renderMessageQuickReplies(attrs);
                                            ha$templateStructure = ha$templateTempQuick.insertAfter(ha$templateStructure);

                                            ha$templateTempQuick.find('.hana-quickreply').slick({
                                                infinite: false,
                                                speed: 300,
                                                slidesToShow: 1,
                                                variableWidth: true
                                            });
                                            hanaCore.quickreplies.push(ha$templateTempQuick);

                                        } else {
                                            attrs.payload = attachment.message.quick_replies;
                                            ha$templateStructure = that.renderMessageQuickReplies(attrs);
                                            hanaCore.quickreplies.push(ha$templateStructure);
                                            if (prepend == 'prepend') {
                                                that.ha$content.prepend(ha$templateStructure);
                                            } else {
                                                that.ha$content.append(ha$templateStructure);
                                            }
                                            ha$templateStructure.find('.hana-quickreply').slick({
                                                infinite: false,
                                                speed: 300,
                                                slidesToShow: 1,
                                                variableWidth: true
                                            });
                                        }
                                    }


                                } else { //text
                                    attrs.message = attachment.message.text;
                                    ha$templateStructure = that.renderMessageText(attrs);
                                    if (!hanaCore.sky_first_msg_store) {
                                        that.renderCollapseText(attrs);
                                        hanaCore.sky_first_msg_store = true;
                                    }
                                    if (prepend == 'prepend') {
                                        that.ha$content.prepend(ha$templateStructure);
                                    } else {
                                        that.ha$content.append(ha$templateStructure);
                                    }
                                }
                            } else {
                                if (attachment.message.attachment.type == 'image') { //image
                                    attrs.pathurl = attachment.message.attachment.payload.url;
                                    attrs.filename = '';
                                    attrs.payload = attachment.message.attachment.payload;
                                    ha$templateStructure = that.renderMessageImage(attrs);
                                    if (prepend == 'prepend') {
                                        that.ha$content.prepend(ha$templateStructure);
                                    } else {
                                        that.ha$content.append(ha$templateStructure);
                                    }
                                } else if (attachment.message.attachment.type == 'template') { //structure
                                    if (attachment.message.attachment.payload.template_type === 'button' ) {
                                        attrs.payload = attachment.message.attachment.payload;
                                        attrs.template_type = 'button';
                                        ha$templateStructure = that.renderMessageStructure(attrs);
                                        if (prepend == 'prepend') {
                                            that.ha$content.prepend(ha$templateStructure);
                                        } else {
                                            that.ha$content.append(ha$templateStructure);
                                        }
                                    } else {
                                        attrs.payload = attachment.message.attachment.payload;
                                        attrs.template_type = 'generic';
                                        ha$templateStructure = that.renderMessageStructure(attrs);
                                        if (prepend == 'prepend') {
                                            that.ha$content.prepend(ha$templateStructure);
                                        } else {
                                            that.ha$content.append(ha$templateStructure);
                                        }
                                    }
                                }
                                if (ha$templateStructure.find('.hana-gallary')) {
                                    that.slickSlider(ha$templateStructure.find('.hana-gallary'));
                                }
                                if (attachment.message.quick_replies) { //quick replies
                                    if (attrs.sendtype == 'storemsg') {
                                        if (ha$('.mideas-message-chat').find('li').length == 1) {
                                            attrs.payload = attachment.message.quick_replies;
                                            var ha$templateTempQuick = that.renderMessageQuickReplies(attrs);
                                            ha$templateStructure = ha$templateTempQuick.insertAfter(ha$templateStructure);
                                            ha$templateTempQuick.find('.hana-quickreply').slick({
                                                infinite: false,
                                                speed: 300,
                                                slidesToShow: 1,
                                                variableWidth: true
                                            });
                                            hanaCore.quickreplies.push(ha$templateTempQuick);

                                        }

                                    } else {
                                        attrs.payload = attachment.message.quick_replies;
                                        var ha$templateTempQuick = that.renderMessageQuickReplies(attrs);
                                        ha$templateStructure = ha$templateTempQuick.insertAfter(ha$templateStructure);
                                        ha$templateTempQuick.find('.hana-quickreply').slick({
                                            infinite: false,
                                            speed: 300,
                                            slidesToShow: 1,
                                            variableWidth: true
                                        });
                                        hanaCore.quickreplies.push(ha$templateTempQuick);

                                    }

                                }
                            }

                        } else {

                            if (attachment.message.text) {
                                if (attachment.message.quick_replies) { //quick replies
                                    if (attrs.sendtype == 'storemsg') {
                                        var ha$templateTemp;
                                        attrs.message = attachment.message.text;
                                        attrs.payload = attachment.message.quick_replies;
                                        if (attrs.message instanceof Object) {
                                            if (attrs.message.type == 'template') {
                                                attrs.payload = attachment.message.text.payload;
                                                ha$templateTemp = that.renderMessageStructure(attrs);
                                            } else if (attrs.message.type == 'image') {
                                                attrs.pathurl = attachment.message.text.payload.url;
                                                attrs.filename = '';
                                                attrs.payload = attachment.message.text.payload;
                                                ha$templateTemp = that.renderMessageImage(attrs);
                                            }
                                        } else {
                                            ha$templateTemp = that.renderMessageText(attrs);
                                        }
                                        // var ha$templateTemp = that.renderMessageText(attrs);
                                        // that.renderCollapseText(attrs);
                                        // hanaCore.quickreplies.push(ha$templateTemp);
                                        ha$templateStructure = ha$templateTemp.insertAfter(ha$templateStructure);
                                        if (ha$templateStructure.find('.hana-gallary')) {
                                            that.slickSlider(ha$templateStructure.find('.hana-gallary'));
                                        }
                                        //render quickreplies
                                        if (ha$('.mideas-message-chat').find('li').length == 1) {
                                            var ha$templateTempQuick = that.renderMessageQuickReplies(attrs);
                                            ha$templateStructure = ha$templateTempQuick.insertAfter(ha$templateStructure);
                                            //insert quickreplies for collapse
                                            ha$templateTempQuick.find('.hana-quickreply').slick({
                                                infinite: false,
                                                speed: 300,
                                                slidesToShow: 1,
                                                variableWidth: true
                                            });
                                            hanaCore.quickreplies.push(ha$templateTempQuick);
                                        }
                                    } else {
                                        if (attachment.message.text != undefined && attachment.message.text != '') {
                                            attrs.message = attachment.message.text;
                                            attrs.payload = attachment.message.quick_replies;
                                            var ha$templateTemp;
                                            if (attrs.message instanceof Object) {
                                                if (attrs.message.type == 'template') {
                                                    attrs.payload = attachment.message.text.payload;
                                                    ha$templateTemp = that.renderMessageStructure(attrs);
                                                } else if (attrs.message.type == 'image') {
                                                    attrs.pathurl = attachment.message.text.payload.url;
                                                    attrs.filename = '';
                                                    attrs.payload = attachment.message.text.payload;
                                                    ha$templateTemp = that.renderMessageImage(attrs);
                                                }
                                            } else {
                                                ha$templateTemp = that.renderMessageText(attrs);
                                            }
                                            // var ha$templateTemp = that.renderMessageText(attrs);
                                            // hanaCore.quickreplies.push(ha$templateTemp);
                                            ha$templateStructure = ha$templateTemp.insertAfter(ha$templateStructure);
                                            if (ha$templateStructure.find('.hana-gallary')) {
                                                that.slickSlider(ha$templateStructure.find('.hana-gallary'));
                                            }
                                            var ha$templateTempQuick = that.renderMessageQuickReplies(attrs);

                                            ha$templateStructure = ha$templateTempQuick.insertAfter(ha$templateStructure);

                                            ha$templateTempQuick.find('.hana-quickreply').slick({
                                                infinite: false,
                                                speed: 300,
                                                slidesToShow: 1,
                                                variableWidth: true
                                            });
                                            hanaCore.quickreplies.push(ha$templateTempQuick);
                                        } else {
                                            attrs.payload = attachment.message.quick_replies;
                                            var ha$templateTempQuick = that.renderMessageQuickReplies(attrs);
                                            hanaCore.quickreplies.push(ha$templateTempQuick);
                                            ha$templateStructure = ha$templateTempQuick.insertAfter(ha$templateStructure);
                                            ha$templateTempQuick.find('.hana-quickreply').slick({
                                                infinite: false,
                                                speed: 300,
                                                slidesToShow: 1,
                                                variableWidth: true
                                            });
                                        }
                                    }
                                } else { //text
                                    attrs.message = attachment.message.text;
                                    var ha$templateTemp = that.renderMessageText(attrs);
                                    if (!hanaCore.sky_first_msg_store) {
                                        that.renderCollapseText(attrs);
                                        hanaCore.sky_first_msg_store = true;
                                    }

                                    ha$templateStructure = ha$templateTemp.insertAfter(ha$templateStructure);
                                }
                            } else {
                                if (attachment.message.attachment.type == 'image') { //image
                                    attrs.pathurl = attachment.message.attachment.payload.url;
                                    attrs.payload = attachment.message.attachment.payload;
                                    attrs.filename = '';
                                    var ha$templateTemp = that.renderMessageImage(attrs);
                                    ha$templateStructure = ha$templateTemp.insertAfter(ha$templateStructure);
                                } else if (attachment.message.attachment.type == 'template') { //structure
                                    if (attachment.message.attachment.payload.template_type === 'button' ) {
                                        attrs.payload = attachment.message.attachment.payload;
                                        attrs.template_type = 'button';
                                        var ha$templateTemp = that.renderMessageStructure(attrs);
                                        ha$templateStructure = ha$templateTemp.insertAfter(ha$templateStructure);
                                        if (ha$templateStructure.find('.hana-gallary')) {
                                            that.slickSlider(ha$templateStructure.find('.hana-gallary'));
                                        }
                                    } else {
                                        attrs.payload = attachment.message.attachment.payload;
                                        attrs.template_type = 'generic';
                                        var ha$templateTemp = that.renderMessageStructure(attrs);
                                        ha$templateStructure = ha$templateTemp.insertAfter(ha$templateStructure);
                                        if (ha$templateStructure.find('.hana-gallary')) {
                                            that.slickSlider(ha$templateStructure.find('.hana-gallary'));
                                        }
                                    }

                                }
                                if (attachment.message.quick_replies) { //quick replies
                                    if (attrs.sendtype == 'storemsg') {
                                        if (ha$('.mideas-message-chat').find('li').length == 1) {
                                            attrs.payload = attachment.message.quick_replies;
                                            var ha$templateTempQuick = that.renderMessageQuickReplies(attrs);
                                            ha$templateStructure = ha$templateTempQuick.insertAfter(ha$templateStructure);
                                            ha$templateTempQuick.find('.hana-quickreply').slick({
                                                infinite: false,
                                                speed: 300,
                                                slidesToShow: 1,
                                                variableWidth: true
                                            });
                                            hanaCore.quickreplies.push(ha$templateTempQuick);

                                        }
                                    } else {
                                        attrs.payload = attachment.message.quick_replies;
                                        var ha$templateTempQuick = that.renderMessageQuickReplies(attrs);
                                        ha$templateStructure = ha$templateTempQuick.insertAfter(ha$templateStructure);
                                        ha$templateTempQuick.find('.hana-quickreply').slick({
                                            infinite: false,
                                            speed: 300,
                                            slidesToShow: 1,
                                            variableWidth: true
                                        });
                                        hanaCore.quickreplies.push(ha$templateTempQuick);
                                    }

                                }
                            }
                        }
                    });

                    return;
                },
                slickSlider: function ($el) {
                    $el.slick({
                        infinite: false,
                        speed: 300,
                        slidesToShow: 1,
                        variableWidth: true
                    });
                },
                renderMessageText: function (attrs) {
                    var text = attrs.message,
                        // match = text.match(/^\/(.*?)(?: (.*))?ha$/),
                        fullname = this.model.get('fullname') || attrs.fullname,
                        extra_classes = attrs.delayed && 'delayed' || '',
                        template, username, template_suggestion,
                        suggestion_value, template_sugg_return;

                    if (attrs.sender === hanaCore.sky_myname) {
                        text = text.replace(/^\/me/, '');
                        template = tpl_msgtextme;
                        username = attrs.sender;
                    } else {
                        template = tpl_msgtexttheir;
                        username = attrs.sender === hanaCore.sky_myname && __(hanaCore.sky_myname) || fullname;
                    }

                    this.ha$content.find('div.chat-event').remove();

                    var ha$temp =  ha$(template(
                        ha_.extend(this.getExtraMessageTemplateAttributes(attrs), {
                            'msgid': attrs.msgid,
                            'sender': attrs.sender,
                            'time': '',
                            'isodate': attrs.timesend,
                            'background': hanaCore.bgColorcontent,
                            'colorcontent': hanaCore.colorcontent,
                            'colorcontentcskh': hanaCore.bgColorcontentcskh,
                            'sky_icon_main': hanaCore.sky_icon_main,
                            'username': username,
                            'version': window.is_ie,
                            'message': this.texthtmlDecode(text),
                            'suggestions': [],
                            'extra_classes': extra_classes
                        })
                    ));
                    ha$temp.find('.mideas-chat-msg-content').addHyperlinks().addEmoticons();
                    return ha$temp;
                },
                renderMessageImage: function (attrs) {
                    var text = attrs.message,
                        // match = text.match(/^\/(.*?)(?: (.*))?ha$/),
                        fullname = this.model.get('fullname') || attrs.fullname,
                        extra_classes = attrs.delayed && 'delayed' || '',
                        template, username, template_suggestion,
                        suggestion_value, template_sugg_return;

                    if (attrs.sender === hanaCore.sky_myname) {
                        template = tpl_msgimageme;
                        username = attrs.sender;
                    } else {
                        template = tpl_msgimagetheir;
                        username = attrs.sender === hanaCore.sky_myname && __(hanaCore.sky_myname) || fullname;
                    }
                    this.ha$content.find('div.chat-event').remove();
                    return ha$(template(
                        ha_.extend(this.getExtraMessageTemplateAttributes(attrs), {
                            'msgid': attrs.msgid,
                            'sender': attrs.sender,
                            'time': '',
                            'isodate': attrs.timesend,
                            'sky_icon_main': hanaCore.sky_icon_main,
                            'timesendorg': attrs.timesend,
                            'username': username,
                            'message': '',
                            'pathurl': attrs.payload.url,
                            'filename': attrs.payload.name,
                            'extra_classes': extra_classes
                        })
                    ));
                },
                renderMessageStructure: function (attrs) {
                    var that  = this;
                    var text = attrs.message,
                        fullname = this.model.get('fullname') || attrs.fullname,
                        extra_classes = attrs.delayed && 'delayed' || '',
                        template, username, template_suggestion,
                        ha$templateStructure,
                        ha$suggestionParent;
                    var template = tpl_msggallery;
                    ha$suggestionParent = ha$(template(
                        ha_.extend(this.getExtraMessageTemplateAttributes(attrs), {
                            'msgid': attrs.msgid,
                            'isodate': attrs.timesend,
                            'timesendorg': attrs.timesend,

                        })
                    ));
                    // console_bk.log('renderMessageStructure ' + JSON.stringify(attrs));
                    if (attrs.template_type === 'button') {
                        var value = attrs.payload;
                        var viewSugg = new hanaCore.AttachmentView({
                            model: ha_.extend(that.getExtraMessageTemplateAttributes(attrs), {
                                'msgid': attrs.msgid,
                                'sender': attrs.sender,
                                'time': '',
                                'isodate': attrs.timesend,
                                'sky_icon_main': hanaCore.sky_icon_main,
                                'username': username,
                                'message': '',
                                'that': that,
                                'template_type': attrs.template_type,
                                'attachment': value,
                                'extra_classes': extra_classes
                            })
                        });
                        var ha$viewSuggElem = viewSugg.render();
                        //insert button
                        ha_.each(value.buttons, function (button, ind) {
                            var buttonModel = new hanaCore.MessageButtonModel(
                                ha_.extend({
                                    'msgid': attrs.msgid,
                                    'sender': attrs.sender,
                                    'time': '',
                                    'isodate': attrs.timesend,
                                    'sky_icon_main': hanaCore.sky_icon_main,
                                    'username': username,
                                    'message': '',
                                    'that': that,
                                    'type': button.type,
                                    'url': button.url,
                                    'title': button.title,
                                    'button': button,
                                    'extra_classes': extra_classes
                                }));
                            var viewButton = new hanaCore.MessageButtonView({ model: buttonModel });

                            var ha$tempButton = viewButton.render();
                            ha$viewSuggElem.find('.card__author').append(ha$tempButton);
                        });
                        ha$suggestionParent.find('.hana-gallary').append(ha$viewSuggElem);
                    } else {
                        var listAttach = attrs.payload.elements;
                        var that = this;
                        ha_.each(listAttach, function (value, index) {
                            var viewSugg = new hanaCore.AttachmentView({
                                model: ha_.extend(that.getExtraMessageTemplateAttributes(attrs), {
                                    'msgid': attrs.msgid,
                                    'sender': attrs.sender,
                                    'time': '',
                                    'isodate': attrs.timesend,
                                    'sky_icon_main': hanaCore.sky_icon_main,
                                    'username': username,
                                    'message': '',
                                    'that': that,
                                    'template_type': attrs.template_type,
                                    'attachment': value,
                                    'extra_classes': extra_classes
                                })
                            });
                            var ha$viewSuggElem = viewSugg.render();
                            //insert button
                            ha_.each(value.buttons, function (button, ind) {
                                var buttonModel = new hanaCore.MessageButtonModel(
                                    ha_.extend({
                                        'msgid': attrs.msgid,
                                        'sender': attrs.sender,
                                        'time': '',
                                        'isodate': attrs.timesend,
                                        'sky_icon_main': hanaCore.sky_icon_main,
                                        'username': username,
                                        'message': '',
                                        'that': that,
                                        'type': button.type,
                                        'url': button.url,
                                        'title': button.title,
                                        'button': button,
                                        'extra_classes': extra_classes
                                    }));
                                var viewButton = new hanaCore.MessageButtonView({ model: buttonModel });

                                var ha$tempButton = viewButton.render();
                                ha$viewSuggElem.find('.card__author').append(ha$tempButton);
                            });
                            ha$suggestionParent.find('.hana-gallary').append(ha$viewSuggElem);
                        });
                    }
                    return ha$suggestionParent;
                },
                renderMessageQuickReplies: function (attrs) {
                    var ha$suggestionParent;
                    var listAttach = attrs.payload;
                    //console_bk.log('QUICK REPLY ' + JSON.stringify(listAttach));
                    var template = tpl_msgquick;
                    return ha$(template(
                        ha_.extend(this.getExtraMessageTemplateAttributes(attrs), {
                            'msgid': attrs.msgid,
                            'sender': attrs.sender,
                            'time': '',
                            'background': hanaCore.bgColorcontent,
                            'colorcontent': hanaCore.colorcontent,
                            'colorcontentcskh': hanaCore.bgColorcontentcskh,
                            'isodate': attrs.timesend,
                            'sky_icon_main': hanaCore.sky_icon_main,
                            'quicks': listAttach,
                            'suggestions': [],
                        })
                    ));
                    // this.renderAutoComplete(lookup);
                    // return ha$suggestionParent;
                },
                insertMessageReceiveStore: function (attrs, prepend) {
                    if (!hanaCore.sky_set_name) {
                        if (attrs.sender != hanaCore.sky_myname) {
                            hanaCore.sky_set_name = true;
                            hanaCore.log('AGENT NAME ' + JSON.stringify(attrs));
                            // this.$el.find(".mideas-name").html(attrs.fullname);
                        }
                    }

                    var insert = prepend ? this.ha$content.prepend : this.ha$content.append;
                    if (attrs.pattern == 'attachment') {
                        var insert = prepend ? this.ha$content.prepend : this.ha$content.append;
                        ha_.compose(
                            this.scrollDownMessageHeight.bind(this),
                            function ($el) {
                            }.bind(this)
                        )(this.renderMessageAttachmentReceive(attrs, 'prepend'));
                    } else if (attrs.pattern == 'suggestions') {
                        return;
                    } else {
                        ha_.compose(
                            this.scrollDownMessageHeight.bind(this),
                            function ($el) {
                                hanaCore.log('prependdddddddddd ' + prepend);
                                $el.find('.loading-image').remove();
                                insert.call(this.ha$content, $el);
                                return $el;
                            }.bind(this)
                        )(this.renderMessageReceiveStore(attrs));
                    }

                },
                renderMessageReceive: function (attrs) {
                    // var msg_time = moment(attrs.time) || moment,
                    var text = attrs.message,
                        // match = text.match(/^\/(.*?)(?: (.*))?ha$/),
                        fullname = this.model.get('fullname') || attrs.fullname,
                        extra_classes = attrs.delayed && 'delayed' || '',
                        template, username, template_suggestion,
                        suggestion_value, template_sugg_return;
                    var messageJson = JSON.parse(hanaCore.base64Decode(attrs.message));
                    if (messageJson.type === 'images') {
                        attrs.pattern = 'image';
                    } else if (messageJson.type === 'attachment') {
                        attrs.pattern = 'attachment';
                    } else if (messageJson.type === 'postback') {
                        attrs.pattern = 'postback';
                    } else {
                        attrs.patternn = 'text';
                    }

                    if (attrs.pattern == 'text' || attrs.pattern == 'postback') {
                        if (attrs.sender === hanaCore.sky_myname) {
                            text = text.replace(/^\/me/, '');
                            template = tpl_msgtextme;
                            username = attrs.sender;
                        } else {
                            template = tpl_msgtexttheir;
                            username = attrs.sender === hanaCore.sky_myname && __(hanaCore.sky_myname) || fullname;
                        }
                    } else if (attrs.pattern == 'image') {
                        if (attrs.sender === hanaCore.sky_myname) {
                            template = tpl_msgimageme;
                            username = attrs.sender;
                        } else {
                            template = tpl_msgimagetheir;
                            username = attrs.sender === hanaCore.sky_myname && __(hanaCore.sky_myname) || fullname;
                        }
                    } else if (attrs.pattern == 'file') {
                        if (attrs.sender === hanaCore.sky_myname) {
                            template = tpl_msgfileme;
                            username = attrs.sender;
                        } else {
                            template = tpl_msgfiletheir;
                            username = attrs.sender === hanaCore.sky_myname && __(hanaCore.sky_myname) || fullname;
                        }
                    } else if (attrs.pattern == 'attachment') {
                        if (attrs.sender === hanaCore.sky_myname) {
                            template = tpl_msgfileme;
                            username = attrs.sender;
                        } else {
                            template = tpl_msgfiletheir;
                            username = attrs.sender === hanaCore.sky_myname && __(hanaCore.sky_myname) || fullname;
                        }
                    }

                    this.ha$content.find('div.chat-event').remove();

                    var temlateSuggestion;
                    hanaCore.log("CHAT BOT" + username);

                    if (attrs.pattern == 'text' || attrs.pattern == 'postback') {
                        var textjson, bodytext;
                        if (attrs.pattern == 'text') {
                            textjson = JSON.parse(hanaCore.base64Decode(text));
                            bodytext = textjson.body;
                            attrs.message = textjson.body;
                        } else {
                            textjson = JSON.parse(hanaCore.base64Decode(text));
                            bodytext = textjson.body.title;
                        }
                        // console_bk.log('BODY REALTIME '+ bodytext + ' PAYLOAD ' + attrs.pattern);
                        this.renderCollapseText(attrs);
                        var ha$temp =  ha$(template(
                            ha_.extend(this.getExtraMessageTemplateAttributes(attrs), {
                                'msgid': attrs.msgid,
                                'sender': attrs.sender,
                                'time': '',
                                'background': hanaCore.bgColorcontent,
                                'colorcontent': hanaCore.colorcontent,
                                'colorcontentcskh': hanaCore.bgColorcontentcskh,
                                'isodate': attrs.timesend,
                                'sky_icon_main': hanaCore.sky_icon_main,
                                'username': username,
                                'message': this.texthtmlDecode(bodytext),
                                'version': window.is_ie,
                                'suggestions': [],
                                'extra_classes': extra_classes
                            })
                        ));
                        ha$temp.find('.mideas-chat-msg-content').addHyperlinks().addEmoticons();
                        return ha$temp;
                    } else if (attrs.pattern == 'image') {
                        // var attachmentEle = JSON.parse(this.base64Decode(attrs.attachment));
                        return ha$(template(
                            ha_.extend(this.getExtraMessageTemplateAttributes(attrs), {
                                'msgid': attrs.msgid,
                                'sender': attrs.sender,
                                'time': '',
                                'isodate': attrs.timesend,
                                'sky_icon_main': hanaCore.sky_icon_main,
                                'timesendorg': attrs.timesend,
                                'username': username,
                                'message': '',
                                // 'pathurl': attachmentEle.attachment.payload.url,
                                // 'filename': attachmentEle.attachment.payload.name,
                                'type_image': 'single',
                                'pathurl': messageJson.img_url[0],
                                'extra_classes': extra_classes
                            })
                        ));
                    } else if (attrs.pattern == 'file') {
                        var attachmentEle = JSON.parse(hanaCore.base64Decode(attrs.attachment));
                        return ha$(template(
                            ha_.extend(this.getExtraMessageTemplateAttributes(attrs), {
                                'msgid': attrs.msgid,
                                'sender': attrs.sender,
                                'time': '',
                                'isodate': attrs.timesend,
                                'sky_icon_main': hanaCore.sky_icon_main,
                                'timesendorg': attrs.timesend,
                                'username': username,
                                'message': '',
                                'pathurl': attachmentEle.attachment.payload.url,
                                'filename': attachmentEle.attachment.payload.name,
                                'extra_classes': extra_classes
                            })
                        ));
                    } else if (attrs.pattern == 'attachment') {
                        var textjson = JSON.parse(hanaCore.base64Decode(text));
                        hanaCore.log('ATTACMENT FILE ' + JSON.stringify(textjson));
                        var url = JSON.parse(textjson.body).attachments[0].payload.url;
                        return ha$(template(
                            ha_.extend(this.getExtraMessageTemplateAttributes(attrs), {
                                'msgid': attrs.msgid,
                                'sender': attrs.sender,
                                'time': '',
                                'isodate': attrs.timesend,
                                'sky_icon_main': hanaCore.sky_icon_main,
                                'timesendorg': attrs.timesend,
                                'username': username,
                                'message': '',
                                'pathurl': url,
                                'filename': '',
                                'extra_classes': extra_classes
                            })
                        ));
                    } else {

                    }

                },
                showMessageSend: function (attrs) {
                    var msg_dates, idx,
                        ha$first_msg = this.ha$content.children('.mideas-chat-message:first'), //get first message
                        first_msg_date = ha$first_msg.data('isodate'), //first message date
                        timeMsg = attrs.timesend,
                        current_msg_date = timeMsg, //current date
                        last_msg_date = this.ha$content.children('.mideas-chat-message:last').data('isodate'); //last message date

                    this.insertMessageSend(attrs);
                },
                insertMessageSend: function (attrs, prepend) {
                    var insert = prepend ? this.ha$content.prepend : this.ha$content.append;
                    var ha$sendMsg = this.renderMessageSend(attrs);
                    this.ha$content.append(ha$sendMsg);
                    this.scrollDown();
                },
                renderMessageSend: function (attrs) {

                    // var msg_time = moment(attrs.time) || moment,
                    var text = attrs.message,
                        fullname = this.model.get('fullname') || attrs.fullname,
                        extra_classes = attrs.delayed && 'delayed' || '',
                        template, username;

                    if (attrs.pattern) {
                        var messageJson = JSON.parse(hanaCore.base64Decode(attrs.message));
                        if (messageJson.type === 'images') {
                            attrs.pattern = 'image';
                        } else if (messageJson.type === 'attachment') {
                            attrs.pattern = 'attachment';
                        } else if (messageJson.type === 'postback') {
                            attrs.pattern = 'postback';
                        } else {
                            attrs.patternn = 'text';
                        }
                    }

                    if (attrs.pattern == 'text' || attrs.pattern == 'postback') {
                        if (attrs.sender === hanaCore.sky_myname) {
                            text = text.replace(/^\/me/, '');
                            template = tpl_msgtextme;
                            username = attrs.sender;
                        } else {
                            template = tpl_msgtexttheir;
                            username = attrs.sender === hanaCore.sky_myname && __(hanaCore.sky_myname) || fullname;
                        }
                    } else if (attrs.pattern == 'image') {
                        if (attrs.sender === hanaCore.sky_myname) {
                            template = tpl_msgimageme;
                            username = attrs.sender;
                        } else {
                            template = tpl_msgimagetheir;
                            username = attrs.sender === hanaCore.sky_myname && __(hanaCore.sky_myname) || fullname;
                        }
                    } else if (attrs.pattern == 'file') {
                        if (attrs.sender === hanaCore.sky_myname) {
                            template = hanaCore.templates.action_file_me;
                            username = attrs.sender;
                        } else {
                            template = hanaCore.templates.action_file_hana;
                            username = attrs.sender === hanaCore.sky_myname && __(hanaCore.sky_myname) || fullname;
                        }
                    } else if (attrs.pattern == 'attachment') {
                        if (attrs.sender === hanaCore.sky_myname) {
                            template = hanaCore.templates.action_file_me;
                            username = attrs.sender;
                        } else {
                            template = hanaCore.templates.action_file_hana;
                            username = attrs.sender === hanaCore.sky_myname && __(hanaCore.sky_myname) || fullname;
                        }
                    }
                    this.ha$content.find('div.chat-event').remove();

                    hanaCore.log("CHAT BOT" + username);
                    if (attrs.pattern == 'text' || attrs.pattern == 'postback') {
                        var textjson, bodytext;
                        if (attrs.pattern == 'text') {
                            textjson = JSON.parse(hanaCore.base64Decode(text));
                            bodytext = textjson.body;
                            attrs.message = textjson.body;
                        } else {
                            textjson = JSON.parse(hanaCore.base64Decode(text));
                            bodytext = textjson.body.title;
                        }
                        // var textjson = JSON.parse(hanaCore.base64Decode(text));
                        var ha$temp = ha$(template(
                            ha_.extend(this.getExtraMessageTemplateAttributes(attrs), {
                                'msgid': attrs.msgid,
                                'sender': attrs.sender,
                                'time': '',
                                'isodate': attrs.timesend,
                                'background': hanaCore.bgColorcontent,
                                'colorcontent': hanaCore.colorcontent,
                                'colorcontentcskh': hanaCore.bgColorcontentcskh,
                                'sky_icon_main': hanaCore.sky_icon_main,
                                'username': username,
                                'version': window.is_ie,
                                'suggestions': [],
                                'message': this.texthtmlDecode(bodytext),
                                'extra_classes': extra_classes
                            })
                        ));
                        ha$temp.find('.mideas-chat-msg-content').addHyperlinks().addEmoticons();
                        return ha$temp;
                    } else if (attrs.pattern == 'image') {
                        hanaCore.log('BASE644444444444 ' + attrs.attachment);
                        var textjson = JSON.parse(hanaCore.base64Decode(attrs.attachment));
                        return ha$(template(
                            ha_.extend(this.getExtraMessageTemplateAttributes(attrs), {
                                'msgid': attrs.msgid,
                                'sender': attrs.sender,
                                'time': '',
                                'isodate': attrs.timesend,
                                'sky_icon_main': hanaCore.sky_icon_main,
                                'timesendorg': attrs.timesend,
                                'username': username,
                                'message': '',
                                'pathurl': textjson.attachment.payload.url,
                                'filename': textjson.attachment.payload.name,
                                'extra_classes': extra_classes
                            })
                        ));
                    } else if (attrs.pattern == 'file') {
                        var textjson = JSON.parse(hanaCore.base64Decode(attrs.attachment));
                        return ha$(template(
                            ha_.extend(this.getExtraMessageTemplateAttributes(attrs), {
                                'msgid': attrs.msgid,
                                'sender': attrs.sender,
                                'time': '',
                                'isodate': attrs.timesend,
                                'sky_icon_main': hanaCore.sky_icon_main,
                                'timesendorg': attrs.timesend,
                                'username': username,
                                'message': '',
                                'pathurl': textjson.attachment.payload.url,
                                'filename': textjson.attachment.payload.name,
                                'extra_classes': extra_classes
                            })
                        ));
                    } else if (attrs.pattern == 'attachment') {
                        var textjson = JSON.parse(hanaCore.base64Decode(text));
                        hanaCore.log('ATTACMENT FILE ' + JSON.stringify(textjson));
                        var url = JSON.parse(textjson.body).attachments[0].payload.url;
                        return ha$(template(
                            ha_.extend(this.getExtraMessageTemplateAttributes(attrs), {
                                'msgid': attrs.msgid,
                                'sender': attrs.sender,
                                'time': '',
                                'isodate': attrs.timesend,
                                'sky_icon_main': hanaCore.sky_icon_main,
                                'timesendorg': attrs.timesend,
                                'username': username,
                                'message': '',
                                'pathurl': url,
                                'filename': '',
                                'extra_classes': extra_classes
                            })
                        ));
                    } else {

                    }

                },
                texthtmlDecode: function (value) {
                    return ha$("<textarea/>").html(value).text();
                },
                texthtmlEncode: function (value) {
                    return ha$('<textarea/>').text(value).html();
                },
                removeQuickReplies: function () {
                    hanaCore.quickreplies.forEach(function (ha$item) {
                        ha$item.remove();
                    });
                },
                getExtraMessageTemplateAttributes: function (attrs) {
                    return {};
                },
                renderCollapseText: function (attrs) {
                    ha$('.mideas-livechat-arrow-box').html("");
                    ha$('.mideas-livechat-arrow-box').html('<p class="mideas-notifi-msg">' + attrs.message + '</p>');
                },
                sendEventNewConversation: function (e) {
                    var textjson = { "type": "event", "name": "new_conversation", "params": [] };
                    var event = hanaCore.base64Encode(JSON.stringify(textjson));
                    var msgid = hanaCore.connection.getUniqueId();
                    var msg = ha$msg({
                        to: this.model.get('jid'),
                        from: hanaCore.connection.jid,
                        type: 'groupchat',
                        id: msgid
                    }).c("event").t(event);
                    hanaCore.connection.send(msg);
                },
                scrollDownMessageHeight: function (ha$message) {
                    // console_bk.log('SCROOLLL down height');
                    if (ha$message) {
                        // console_bk.log('SCROOLLL down height 11');
                        this.$el.scrollTop(this.ha$contentDiv[0].scrollHeight + ha$message[0].scrollHeight);
                    }
                    return this;
                },
                scrollDown: function () {
                    // this.ha$content.scrollTop(99999);
                    // this.ha$content.animate({ scrollTop: 99999}, 1000);
                    this.$el.scrollTop(this.ha$contentDiv[0].scrollHeight);
                    return this;
                },
                scrollDownSlow: function () {
                    var heightPost = this.ha$contentDiv[0].scrollHeight - this.$el.scrollTop();
                    // console_bk.log('SCROOLLL top ' + this.ha$contentDiv[0].scrollHeight + ' scroll current ' +  this.$el.scrollTop() + ' height ' + heightPost);
                    if (heightPost > 240) {
                        var heightScroll = this.$el.scrollTop() + 240;
                        this.$el.animate({scrollTop: heightScroll},1000);
                    } else {
                        this.$el.animate({scrollTop: this.ha$contentDiv[0].scrollHeight},1000);
                    }
                    return this;
                }

            });
            hanaCore.ChatRoomNotifyView = Backbone.View.extend({
                tagName: 'div',
                className: 'hana-chat-body',
                id: 'hana-chat-body',
                events: {
                    'click .mideas-btn-like': 'likeConversation',
                    'click .mideas-btn-dislike': 'dislikeConversation',
                    'click .hana-quickreply-action': 'sendQuickReply',
                    'click .hana-form-input': 'typePing',
                },

                initialize: function () {
                    this.model.set('message', 'First message');
                    var that = this;
                },

                render: function () {
                    //console_bk.log('RENDER BODYCHAT');
                    var ha$template;
                    ha$template = this.$el.html(tpl_chatroombody(
                        ha_.extend(
                            this.model.toJSON(), {
                                heading: 'CHAO',
                                show_form: hanaCore.showForm
                            })));
                    var cssText = "transform: translateY(0px); padding: 25px 25px 0px;";
                    this.$el.find('.hana-conversation-parts').css("cssText", cssText);
                    return ha$template;


                },
                appendToBody: function () {
                    hanaCore.frameNotifyBody.find('.hana-chat-container').prepend(this.$el);
                    this.ha$content = this.$el.find('.mideas-message-chat');
                    this.ha$contentDiv = this.$el.find('.mideas-wrapper-popup-body');
                    this.changeContent();
                },
                changeContent: function (msg) {
                    this.model.set('message', msg);
                    var template = tpl_msgtextnotifytheir;
                    var ha$temp =  ha$(template(
                        ha_.extend(this.model.toJSON(), {
                            'background': hanaCore.bgColorcontent,
                            'colorcontent': hanaCore.colorcontent,
                            'colorcontentcskh': hanaCore.bgColorcontentcskh
                        })
                    ));
                    this.ha$content.html('');
                    this.ha$content.append(ha$temp);
                },
                intervalNotifyTitle: function () {
                    if (hanaCore.stateVisible === false) {
                        hanaCore.intervalNotifyVar =  setInterval(function(){
                            ha$("title").text('Bạn có tin nhắn mới...');
                            setTimeout(function () {
                                ha$("title").text(hanaCore.pageTitleHtml);
                            }, 1500)
                        }, 3000);
                    }
                },
                hideOrShow: function (message) {
                    hanaCore.log('Đây là message hihihi 111 ' + JSON.stringify(message));
                    if (message) {
                        var messageText = 'Bạn nhận được tin nhắn...';
                        if (message.get('pattern') === 'broadcast') {
                            return;
                        }
                        if (message.get('pattern') === 'text') {
                            this.intervalNotifyTitle();

                            var msgBase = JSON.parse(hanaCore.base64Decode(message.get('message')));
                            if (msgBase.type === 'text') {
                                messageText = msgBase.body;
                            } else if (msgBase.type === 'image') {
                                messageText = 'Bạn nhận được hình ảnh ...';
                            }
                        }
                        if (message.get('pattern') === 'attachment') {
                            this.intervalNotifyTitle();
                            var msgBase = JSON.parse(hanaCore.base64Decode(message.get('message')));
                            console.log('Đây là message hihihi ' + JSON.stringify(msgBase));
                            ha_.each(msgBase, function (msgs) {
                                if (msgs.message.text) {
                                    messageText = msgs.message.text;
                                } else if (msgs.message.attachment && msgs.message.attachment.payload && msgs.message.attachment.type === 'template') {
                                    var payload = msgs.message.attachment.payload;
                                    if (payload.text) {
                                        messageText = payload.text;
                                    }
                                }
                            });
                            /*if (msgBase.type === 'text') {
                                messageText = msgBase.body;
                            } else if (msgBase.type === 'image') {
                                messageText = 'Bạn nhận được hình ảnh ...';
                            }*/
                        }
                        this.changeContent(messageText);
                    }

                    if (hanaCore.mideas_collapse) {
                        // hien cai nay
                        if (hanaCore.checkMobile() === 'mobile') {
                            hanaCore.frameBodyNotifyChild.css("cssText", hanaCore.styleBodyNotifyMobileShow);
                        } else {
                            hanaCore.frameBodyNotifyChild.css("cssText", hanaCore.styleBodyNotifyShow);
                        }
                    } else {
                        //an cai nay
                        if (hanaCore.checkMobile() === 'mobile') {
                            hanaCore.frameBodyNotifyChild.css("cssText", hanaCore.styleBodyMobileHide);
                        } else {
                            hanaCore.frameBodyNotifyChild.css("cssText", hanaCore.styleBodyHide);
                        }

                    }
                }

            });
            hanaCore.ChatRoomNotifyParentView = Backbone.View.extend({
                tagName: 'div',
                id: 'hana-container-body',
                events: {
                    'click .close-notify-collapse': 'closeNotify',
                    'click .hana-open-chat': 'openChatRoom'
                },

                initialize: function () {
                    var that = this;
                },

                render: function () {
                    //console_bk.log('RENDER BODYCHAT');
                    var ha$template;
                    ha$template = this.$el.html(tpl_bodynotifypopup(
                        ha_.extend({}, {
                                heading: 'CHAO',
                                show_form: hanaCore.showForm,
                                domain: hanaCore.resource_popup
                            })));
                    return ha$template;
                },
                closeNotify: function () {
                    hanaCore.log('close notificationnnnn');
                    if (hanaCore.checkMobile() === 'mobile') {
                        hanaCore.frameBodyNotifyChild.css("cssText", hanaCore.styleBodyMobileHide);
                    } else {
                        hanaCore.frameBodyNotifyChild.css("cssText", hanaCore.styleBodyHide);
                    }
                },
                openChatRoom: function () {
                    hanaCore.log('close notificationnnnn');
                    hanaCore.actionCollapse('show');
                }


            });
            hanaCore.MessageButtonModel = Backbone.Model.extend({});
            hanaCore.AttachmentView = Backbone.View.extend({
                // tagName: 'li',
                // className: 'mideas-flexslider',
                events: {
                    'click .mideas-send-suggestion': 'sendSuggestion',
                },

                initialize: function () {
                    // return this.render();
                },

                render: function () {
                    return this.$el.html(tpl_msgcard(this.model));
                }

            });
            hanaCore.MessageButtonView = Backbone.View.extend({
                tagName: "div",
                className: "mideas-button-view card__author-content hana-pointer",
                events: {
                    "click .mideas-button-action": "actionEvent"
                },
                initialize: function () {
                    // this.listenTo(this.model, "change", this.render);
                    // this.listenTo(this.model, 'destroy', this.remove);
                },
                render: function () {
                    return this.$el.html(tpl_msgbutton(
                        ha_.extend(
                            this.model.toJSON(),
                            {
                                background: hanaCore.bgColorcontent,
                                'colorcontent': hanaCore.colorcontent,
                                'colorcontentcskh': hanaCore.bgColorcontentcskh,
                            })));
                },
                sendSuggestion: function (e) {
                    // this.model.destroy();
                },
                remove: function () {
                    this.$el.remove();
                },
                actionEvent: function () {
                    //console_bk.log("This is action" + this.model.get('type'));
                    if (this.model.get('type') == 'web_url') {
                        this.actionTargetUrl();
                    } else if (this.model.get('type') == 'postback') {
                        this.actionPostbackUrl();
                    } else if (this.model.get('type') == 'modify_save') {
                        this.actionModifySave();
                    }
                },
                actionTargetUrl: function () {
                    var win = window.open(this.model.get('url'), '_blank');
                    win.focus();
                },
                actionPostbackUrl: function () {
                    //send message
                    var text = this.model.get('title');
                    var payload = this.model.get('button').payload;
                    try {
                        var jsonPay = JSON.parse(hanaCore.base64Decode(payload));
                        // console_bk.log('trythismodel ' + JSON.stringify(this.model));
                        hanaCore.chatFooterView.onChatRoomMessageSubmitted(text, payload);
                    } catch (e){
                        // console_bk.log('catchthismodel ' + JSON.stringify(this.model));
                        hanaCore.chatFooterView.onChatRoomMessageSubmitted(text);
                    }

                }
            });

            hanaCore.onConnectedChat = function () {
                //console_bk.log('ONCONNECTED GENCHATTTT ');
                hanaCore.getTitleHtml();

                hanaCore.styleBodyShow = 'z-index: 2147483000!important; position: fixed!important; bottom: 0px; '+hanaCore.popup_position+': 20px; height: calc(100% - 20px - 20px); width: '+hanaCore.popup_width+'px!important; min-height: 250px!important; max-height: '+hanaCore.popup_height+'px!important; -webkit-box-shadow: 0 5px 40px rgba(0, 0, 0, .16)!important; box-shadow: 0 5px 40px rgba(0, 0, 0, .16)!important; border-radius: 8px 8px 0 0 !important; overflow: hidden!important; opacity: 1!important';
                hanaCore.styleBodyNotifyShow = 'z-index: 2147483000!important; position: fixed!important; bottom: 70px; '+hanaCore.popup_position+': 1px; height: calc(100% - 20px - 20px); width: '+hanaCore.popup_width+'px!important; min-height: 120px!important; max-height: 160px!important; -webkit-box-shadow: 0 5px 40px rgba(0, 0, 0, .16)!important; box-shadow: none!important; border-radius: 8px 8px 0 0 !important; overflow: hidden!important; opacity: 1!important';
                hanaCore.styleBodyHide = 'z-index: 2147483000!important; position: fixed!important; bottom: 0px; right: -9999em; height: calc(100% - 20px - 20px); width: 370px!important; min-height: 250px!important; max-height: 590px!important; -webkit-box-shadow: 0 5px 40px rgba(0, 0, 0, .16)!important; box-shadow: 0 5px 40px rgba(0, 0, 0, .16)!important; border-radius: 8px 8px 0 0 !important; overflow: hidden!important; opacity: 1!important';
                hanaCore.styleBodyMobileShow = 'z-index: 2147483001!important; width: 100%!important; height: 100%!important; max-height: none!important; top: 0!important; left: 0!important; right: 0!important; bottom: 0!important; border-radius: 0!important;position: fixed !important;';
                hanaCore.styleBodyNotifyMobileShow = 'z-index: 2147483001!important; width: 100%!important; height: 150px!important; max-height: none!important; top: auto!important; left: 0!important; right: 0!important; bottom: 100px!important; border-radius: 0!important;position: fixed !important; box-shadow: none!important;';
                hanaCore.styleBodyMobileHide = 'z-index: 2147483001!important; width: 100%!important; height: 100%!important; max-height: none!important; right: -9999em!important; bottom: 0!important; border-radius: 0!important;position: absolute !important';

                hanaCore.styleCollapse = 'z-index: 2147483000!important; position: fixed!important; bottom: 0px; '+hanaCore.popup_position+': 20px; width: 160px!important; height: 38px!important; -webkit-transition: -webkit-box-shadow 80ms ease-in-out!important; transition: -webkit-box-shadow 80ms ease-in-out!important; transition: box-shadow 80ms ease-in-out!important; transition: box-shadow 80ms ease-in-out, -webkit-box-shadow 80ms ease-in-out!important; -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, .06), 0 2px 32px rgba(0, 0, 0, .16)!important; box-shadow: 0 1px 6px rgba(0, 0, 0, .06), 0 2px 32px rgba(0, 0, 0, .16)!important;border-radius: 0 !important;border-radius: 8px 8px 0 0 !important;';
                hanaCore.styleCollapseCir = 'z-index: 2147483000!important; position: fixed!important; bottom: 20px; '+hanaCore.popup_position+': 20px; width: 70px!important; height: 70px!important; border-radius: 50%!important; -webkit-transition: -webkit-box-shadow 80ms ease-in-out!important; transition: -webkit-box-shadow 80ms ease-in-out!important; transition: box-shadow 80ms ease-in-out!important; transition: box-shadow 80ms ease-in-out, -webkit-box-shadow 80ms ease-in-out!important; -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, .06), 0 2px 32px rgba(0, 0, 0, .16)!important; box-shadow: 0 1px 6px rgba(0, 0, 0, .06), 0 2px 32px rgba(0, 0, 0, .16)!important;';
                hanaCore.styleCollapseHide = 'z-index: 2147483001!important; width: 100%!important; height: 100%!important; max-height: none!important; right: -9999em!important; bottom: 0!important; border-radius: 0!important;position: absolute !important';
                hanaCore.frameBodySpan = ha$('#hana-frame-span');
                hanaCore.frameBodyDivChild = ha$('#hana-div-body');
                hanaCore.frameBodyNotifyChild = ha$('#hana-div-body-msg');
                hanaCore.frameCollapseDiv = ha$('#hana-collapse-iframe')

                hanaCore.frameBody = ha$('#hana-chat-iframe').contents().find('body');
                hanaCore.frameHeader = ha$('#hana-chat-iframe').contents().find('head');
                hanaCore.frameNotifyBody = ha$('#hana-chat-iframe-msg').contents().find('body');
                hanaCore.frameNotifyHeader = ha$('#hana-chat-iframe-msg').contents().find('head');
                hanaCore.frameCollapseBody = hanaCore.frameCollapseDiv.contents().find('body');
                hanaCore.frameCollapseHeader = hanaCore.frameCollapseDiv.contents().find('head');
                var unixtime = Number(Date.now());
                hanaCore.frameHeader.append(tpl_headpopup({
                    heading: 'CHAO',
                    unixtime: unixtime,
                    product: hanaCore.hana_product,
                    domain: hanaCore.resource_popup
                }));

                hanaCore.frameBody.append(tpl_bodypopup({
                    heading: 'CHAO'
                }));

                hanaCore.frameNotifyHeader.append(tpl_headpopup({
                    heading: 'CHAO',
                    unixtime: unixtime,
                    product: hanaCore.hana_product,
                    domain: hanaCore.resource_popup
                }));

                var notifyParentView = new hanaCore.ChatRoomNotifyParentView();
                hanaCore.frameNotifyBody.append(notifyParentView.render());

                hanaCore.frameCollapseHeader.append(tpl_headcollapse({
                    'domain': hanaCore.resource_popup,
                    'product': hanaCore.hana_product
                }));
                hanaCore.frameCollapseBody.append(ha$('<div></div>'));
                hanaCore.all_step.step_connected = true;
                hanaCore.checkwindownvisible(hanaCore.actionVisible);
            };
            hanaCore.actionVisible = function (value) {
                if (value === 'hidden') {
                    hanaCore.stateVisible = false;
                    hanaCore.iqBroadCastAction('hidden');
                } else if (value === 'focus') {
                    hanaCore.log('FOCUSTOTHIS ' + hanaCore.intervalNotifyVar);
                    window.clearInterval(hanaCore.intervalNotifyVar);
                    hanaCore.stateVisible = true;
                    if (hanaCore.pageTitleHtml) {
                        ha$("title").text(hanaCore.pageTitleHtml);
                    }
                    hanaCore.iqBroadCastAction('focus');
                }
            };
            hanaCore.hideOrShowCollapse = function (type) {
                if (type === 'hide') {
                    hanaCore.frameCollapseDiv.css("cssText", hanaCore.styleCollapseHide);
                } else {
                    if (hanaCore.collapse_template === 1) {
                        hanaCore.frameCollapseDiv.css("cssText", hanaCore.styleCollapse);
                    } else {
                        hanaCore.frameCollapseDiv.css("cssText", hanaCore.styleCollapseCir);
                    }
                }
            };
            hanaCore.actionCollapse = function (type) {
                if (type === 'show') {
                    hanaCore.mideas_collapse = false;
                    hanaCore.chatRoomNotifyView.hideOrShow();
                    if (hanaCore.checkMobile() === 'mobile') {
                        hanaCore.turnOnDeviceViewport();
                        hanaCore.frameBodyDivChild.css("cssText", hanaCore.styleBodyMobileShow);
                        hanaCore.frameBodyNotifyChild.css("cssText", hanaCore.styleBodyMobileHide);
                        hanaCore.hideOrShowCollapse('hide');
                    } else {
                        hanaCore.frameBodyDivChild.css("cssText", hanaCore.styleBodyShow);
                        hanaCore.frameBodyNotifyChild.css("cssText", hanaCore.styleBodyHide);
                        hanaCore.hideOrShowCollapse('hide');
                    }
                } else if (type === 'hide'){
                    hanaCore.mideas_collapse = true;
                    hanaCore.chatRoomNotifyView.hideOrShow();
                    if (hanaCore.checkMobile() === 'mobile') {
                        hanaCore.turnOffDeviceViewport();
                        hanaCore.frameBodyDivChild.css("cssText", hanaCore.styleBodyMobileHide);
                        hanaCore.frameBodyNotifyChild.css("cssText", hanaCore.styleBodyMobileHide);
                    } else {
                        hanaCore.frameBodyDivChild.css("cssText", hanaCore.styleBodyHide);
                        hanaCore.frameBodyNotifyChild.css("cssText", hanaCore.styleBodyHide);
                    }
                    hanaCore.hideOrShowCollapse('show');
                } else {
                    if (hanaCore.checkMobile() === 'mobile') {
                        if (hanaCore.mideas_collapse) {
                            hanaCore.turnOffDeviceViewport();
                            hanaCore.frameBodyDivChild.css("cssText", hanaCore.styleBodyMobileHide);
                            hanaCore.frameBodyNotifyChild.css("cssText", hanaCore.styleBodyMobileHide);
                            hanaCore.hideOrShowCollapse('show');
                            type = 'hide';
                        } else {
                            hanaCore.turnOnDeviceViewport();
                            hanaCore.frameBodyDivChild.css("cssText", hanaCore.styleBodyMobileShow);
                            hanaCore.frameBodyNotifyChild.css("cssText", hanaCore.styleBodyMobileHide);
                            hanaCore.hideOrShowCollapse('hide');
                            type = 'show';
                        }
                    } else {
                        if (hanaCore.mideas_collapse) {
                            hanaCore.frameBodyDivChild.css("cssText", hanaCore.styleBodyHide);
                            hanaCore.frameBodyNotifyChild.css("cssText", hanaCore.styleBodyHide);
                            hanaCore.hideOrShowCollapse('show');
                            type = 'hide';
                        } else {
                            hanaCore.frameBodyDivChild.css("cssText", hanaCore.styleBodyShow);
                            hanaCore.frameBodyNotifyChild.css("cssText", hanaCore.styleBodyHide);
                            hanaCore.hideOrShowCollapse('hide');
                            type = 'show';
                        }
                    }
                }
                if (type === 'show') {
                    hanaCore.iqBroadCastAction('show');
                } else {
                    hanaCore.iqBroadCastAction('hide');
                }
            };

            hanaCore.iqBroadCastAction = function (type, msg) {
                var modelRoom = hanaCore.chatBoxModel;
                var dataSend = {};
                if (type === 'hide') {
                    dataSend = {
                        type: 'collapse',
                        data: {
                            status: 'hide'
                        }
                    };
                } else if (type === 'show') {
                    dataSend = {
                        type: 'collapse',
                        data: {
                            status: 'show'
                        }
                    };
                } else if (type === 'hidden') {
                    dataSend = {
                        type: 'visible',
                        data: {
                            status: 'hidden'
                        }
                    };
                } else if (type === 'focus') {
                    dataSend = {
                        type: 'visible',
                        data: {
                            status: 'focus'
                        }
                    };
                } else if (type === 'untyping') {
                    hanaCore.log('uuuuuuuuuntyping');
                    hanaCore.typingstatus = false;
                    dataSend = {
                        type: 'typing',
                        data: {
                            status: 'untyping',
                            username: Strophe.getNodeFromJid(hanaCore.connection.jid),
                            content: ''
                        }
                    }
                } else if (type === 'typing') {
                    hanaCore.log('ttttttttttttyping');
                    if (hanaCore.typingstatus) {
                        return false;
                    }
                    hanaCore.typingstatus = true;
                    setTimeout(function () {
                        hanaCore.typingstatus = false;
                    }, 3000);
                    dataSend = {
                        type: 'typing',
                        data: {
                            status: 'typing',
                            username: Strophe.getNodeFromJid(hanaCore.connection.jid),
                            content: msg
                        }
                    }
                }
                var iq = ha$iq({
                    to: modelRoom.get('jid') + hanaCore.hana_room,
                    type: "set",
                    id: Math.random().toString(36).substr(2, 6)
                }).c("query", {
                    xmlns: "broadcast"
                }).t(hanaCore.base64Encode(JSON.stringify(dataSend)));
                // console_bk.log('SENDBROADCASTIQ ' + type);
                hanaCore.connection.sendIQ(iq, function () {}.bind(this), function () {}.bind(this));
            };

            var checkUserChat = function () {
                if (window.notsupport) {
                    return;
                }
                hanaCore.renderFinger(function (users) {
                    var uid = "web_" + hanaCore.hana_appid + "hanahana" + users;
                    hanaCore.node_jid = uid;
                    hanaCore.webClient(uid, hanaCore.chatAction.webClientCB);
                    hanaCore.getIntegration(hanaCore.chatAction.integrationCB, 2);
                    hanaCore.getIntegration(hanaCore.chatAction.integrationCBFB, 1);
                    hanaCore.listRoom(uid, hanaCore.chatAction.listRoomCB);
                    hanaCore.getCustomerInfo(uid, hanaCore.chatAction.customerInfoCB);

                    //request dong bo cac api kia
                })
            };
            hanaCore.chatAction = {
                webClientCB: function (data) {
                    //console_bk.log('webClientCB ' + JSON.stringify(data));
                    if (data) {
                        var json = data;
                        if (json.code == 200) {
                            hanaCore.chatAction.updateAgentToSetting(json);
                        }
                    }
                },
                customerInfoCB: function (data) {
                    //console_bk.log('customerInfoCB ' + JSON.stringify(data));
                    if (data) {
                        var json = data;
                        if (json.errorCode == 200) {
                            if (json.data.length > 0) {
                                hanaCore.isGetcustomerinfo = true;
                            } else {
                                if (hanaCore.setUserInfo) {
                                    try {
                                        var a = hanaCore.setUserInfoData;
                                        hanaCore.saveCustomer(a.name, a.email, a.phone, a.address, function () { });
                                    } catch (e){

                                    }

                                }
                            }
                            hanaCore.all_step.step_getcustomer = true;
                            //console_bk.log('CALL EMIT ROOM customerInfoCB');
                            hanaCore.emit("allDone");
                        }
                    }
                },
                integrationCB: function (data) {
                    //console_bk.log('integrationCB ' + JSON.stringify(data));
                    if (data) {
                        var json = data;
                        if (json.background_color_content) {
                            hanaCore.bgColorcontent = json.background_color_content;
                        } else {
                            hanaCore.bgColorcontent = '#0a7eb7;';
                        }
                        if (json.background_color_content_cskh) {
                            hanaCore.bgColorcontentcskh = json.background_color_content_cskh;
                        } else {
                            hanaCore.bgColorcontentcskh = '#f4f7f9;';
                        }
                        if (json.color_content) {
                            hanaCore.colorcontent = json.color_content;
                        } else {
                            hanaCore.colorcontent = '#ffffff';
                        }
                        if (json.background_color) {
                            hanaCore.bgColor = json.background_color;
                        } else {
                            hanaCore.bgColor = '#0a7eb7';
                        }
                        if (json.collapse == '1') {
                            hanaCore.mideas_collapse = true;
                        } else {
                            hanaCore.mideas_collapse = false;
                        }
                        hanaCore.log('require_infomation ' + json.require_infomation);
                        if (json.require_infomation === '1') {
                            hanaCore.mideas_require_infomation = true;
                        } else {
                            hanaCore.mideas_require_infomation = false;
                        }

                        if (json.popup_subtitle) {
                            hanaCore.popup_subtitle = json.popup_subtitle;
                        } else {
                            hanaCore.popup_subtitle = 'Support Specialist';
                        }
                        if (json.dividendchat && (parseInt(json.dividendchat) === hanaCore.DIVIDENT_CHAT.dividend)) {
                            hanaCore.dividend_chat = hanaCore.DIVIDENT_CHAT.dividend;
                        } else {
                            hanaCore.dividend_chat = hanaCore.DIVIDENT_CHAT.sendall;
                        }
                        if (json.require_terms === '1') {
                            hanaCore.require_terms = true;
                            hanaCore.terms_html = json.terms_html;
                        } else {
                            hanaCore.require_terms = false;
                        }
                        if (json.powerby_hide === '1') {
                            hanaCore.powerby_hide = true;
                        } else {
                            hanaCore.powerby_hide = false;
                        }
                        if (json.collapse_template === '1') {
                            hanaCore.collapse_template = 1;
                        } else {
                            hanaCore.collapse_template = 0;
                        }
                        if (json.list_agent_chat) {
                            hanaCore.list_agent_chat = JSON.parse(json.list_agent_chat);
                        }
                        if (json.popup_width) {
                            hanaCore.popup_width = parseInt(json.popup_width);
                        } else {
                            hanaCore.popup_width = 350;
                        }
                        if (json.popup_height) {
                            hanaCore.popup_height = 200 + parseInt(json.popup_height);
                        } else {
                            hanaCore.popup_height = 480;
                        }

                        if (json.popup_position) {
                            hanaCore.popup_position = json.popup_position === '0' ? 'right': 'left';
                        } else {
                            hanaCore.popup_position = 'right';
                        }

                        hanaCore.all_step.step_integration = true;
                        // // console_bk.log('CALL EMIT ROOM INTEGRATE  ' + JSON.stringify(json));
                        hanaCore.emit("allDone");
                    }
                },
                integrationCBFB: function (data) {
                    //console_bk.log('integrationCB ' + JSON.stringify(data));
                    if (data) {
                        var json = data;
                        if (json.readSpeed) {
                            hanaCore.speed_read = parseInt(json.readSpeed);
                        } else {
                            hanaCore.speed_read = 5;
                        }
                        hanaCore.timeOut_read = Math.round(20000 / hanaCore.speed_read);
                    }
                },
                listRoomCB: function (data) {
                    hanaCore.log('listRoomCB ');
                    hanaCore.log('listRoomCB ' + JSON.stringify(data));
                    hanaCore.log('listRoomCB ' + JSON.stringify(hanaCore.dataObject));
                    hanaCore.dataObject.list_room = data;
                    hanaCore.all_step.step_list_room = true;
                    //console_bk.log('CALL EMIT ROOM LISTROOM');
                    hanaCore.emit("allDone");
                    // hanaCore.createChatRoom.create(data);
                },
                updateAgentToSetting: function (data) {
                    if (hanaCore.checkResponesive()) {
                        hanaCore.is_responesive = true;
                    } else {
                        hanaCore.is_responesive = false;
                    }
                    var settingUser = { "all_agent": data };
                    ha_.extend(hanaCore, settingUser);
                    //console_bk.log('updateAgentToSetting ' + JSON.stringify(hanaCore.all_agent));
                    hanaCore.getBotInfo(hanaCore.chatAction.getAgentInfoCB);
                    hanaCore.usernamechat = hanaCore.all_agent.uid;
                    hanaCore.passchat = hanaCore.all_agent.pss;
                    hanaCore.firstConnect = false;
                    hanaCore.logIn(hanaCore.all_agent.uid, hanaCore.all_agent.pss);
                },
                /*getAgentInfo: function () {
                    hanaCore.getBotInfo(this.getAgentInfoCB);
                },*/
                getAgentInfoCB: function (data) {

                    if (data) {
                        var json = data;
                        hanaCore.partner_id = json.partner_id;
                        hanaCore.agent_bot_info = json;
                        hanaCore.sky_ai_name = json.name;
                        hanaCore.sky_ai_desc = json.description;
                        if (json.avatar_url && json.avatar_url != 'null') {
                            hanaCore.sky_icon_main = json.avatar_url;
                        } else {
                            hanaCore.sky_icon_main = 'https://resources.mideasvn.com/mideas/images/hana77.jpg';
                        }
                        hanaCore.all_step.step_get_bot = true;
                        //console_bk.log('CALL EMIT getAgentInfoCB');
                        hanaCore.emit("allDone");
                    }
                },
                openChatRoom: function (room) {
                    room = hanaCore.roomId;
                    //console_bk.log(' EMIT OPEN CHATBOX ' + JSON.stringify(hanaCore.all_step));
                    if (hanaCore.all_step.step_connected && hanaCore.all_step.step_get_bot && hanaCore.all_step.step_integration && hanaCore.all_step.step_list_room && hanaCore.all_step.step_getcustomer) {
                        if (hanaCore.firstConnect) {
                            hanaCore.chatRoomView.listenMessage();
                            //console_bk.log('donereconnected...');
                        } else {
                            hanaCore.firstConnect = true;
                            // console_bk.log(' EMIT OPEN CHATBOX START ' + hanaCore.mideas_require_infomation + ' GETINFO ' + hanaCore.isGetcustomerinfo);
                            hanaCore.appendCssHead();
                            hanaCore.showForm = false;
                            if (hanaCore.mideas_require_infomation) {
                                if (hanaCore.isGetcustomerinfo) {
                                    // vao luong mac dinh
                                } else {
                                    //hoi
                                    hanaCore.showForm = true;
                                }
                            } else {
                                //vao luong mac dinh
                            }
                            hanaCore.chatBoxModel = new hanaCore.ChatBox({
                                'id': room,
                                'jid': room,
                                'name': 'Hỗ trợ',
                                'nick': room,
                                'type': 'chatroom',
                                'is_block': false,
                                'count_block': 0,
                                'rate': '0'
                            });
                            hanaCore.headerModel = new hanaCore.HeaderModel({

                            });
                            hanaCore.footerModel = new hanaCore.FooterModel({
                            });
                            hanaCore.chatFooterView = new hanaCore.ChatFooterView(
                                { model: hanaCore.footerModel }
                            );

                            hanaCore.chatHeaderView = new hanaCore.ChatHeaderView(
                                { model: hanaCore.headerModel }
                            );

                            hanaCore.chatRoomView = new hanaCore.ChatRoomView(
                                { model: hanaCore.chatBoxModel }
                            );
                            hanaCore.chatCollapseView = new hanaCore.ChatCollapseView(
                                { model: hanaCore.chatBoxModel }
                            );
                            hanaCore.chatRoomNotifyView = new hanaCore.ChatRoomNotifyView(
                                { model: hanaCore.chatBoxModel }
                            );

                            //console_bk.log('LOGSSSSSSSG ');
                            hanaCore.chatFooterView.render();
                            hanaCore.chatFooterView.appendToBody();
                            hanaCore.chatRoomView.render();
                            hanaCore.chatRoomView.appendToBody();
                            hanaCore.chatHeaderView.render();
                            hanaCore.chatHeaderView.appendToBody();
                            hanaCore.chatCollapseView.render();

                            hanaCore.chatRoomNotifyView.render();
                            hanaCore.chatRoomNotifyView.appendToBody();
                            hanaCore.actionCollapse();

                        }

                    }
                }
            }
            //console_bk.log("REGISTER CHAT HANA...");
            checkUserChat();
            // hanaCore.on('connected', onConnectedChat);
            hanaCore.on('allDone', hanaCore.chatAction.openChatRoom);



        }
    });
}));
