// hanaCore.js (A browser based XMPP chat client)
// http://hanaCorejs.org
//
// Copyright (c) 2012-2016, Jan-Carel Brand <jc@opkode.com>
// Licensed under the Mozilla Public License (MPLv2)
//
/*global define */

/* This is a hanaCore.js plugin which add support for application-level pings
 * as specified in XEP-0199 XMPP Ping.
 */
(function (root, factory) {
    define("hana-ping", [
        "hana-core",
        "hana-api",
        "strophe.ping"
    ], factory);
}(this, function (hanaCore, hana_api) {
    "use strict";
    // Strophe methods for building stanzas
    var Strophe = hana_api.env.Strophe;
    // Other necessary globals
    var ha_ = hana_api.env.ha_,
    ha$iq = hana_api.env.$iq,
    ha$build = hana_api.env.$build,
    ha$msg = hana_api.env.$msg,
    ha$pres = hana_api.env.$pres;
    
    hana_api.plugins.add('hana-ping', {

        initialize: function () {
            /* The initialize function gets called as soon as the plugin is
             * loaded by hanaCore.js's plugin machinery.
             */
            var hanaCore = this.hanaCore;
            // console_bk.log("ADDPLUGIN hana-ping ...");
            this.updateSettings({
                ping_interval: 30 //in seconds
            });

            hanaCore.ping = function (jid, success, error, timeout) {
                // XXX: We could first check here if the server advertised that
                // it supports PING.
                // However, some servers don't advertise while still keeping the
                // connection option due to pings.
                //
                // var feature = hanaCore.features.findWhere({'var': Strophe.NS.PING});
                hanaCore.lastStanzaDate = new Date();
                if (typeof jid === 'undefined' || jid === null) {
                    jid = Strophe.getDomainFromJid(hanaCore.bare_jid);
                }
                if (typeof timeout === 'undefined' ) { timeout = null; }
                if (typeof success === 'undefined' ) { success = null; }
                if (typeof error === 'undefined' ) { error = null; }
                if (hanaCore.connection) {
                    hanaCore.connection.ping.ping(jid, success, error, timeout);
                    return true;
                }
                return false;
            };

            hanaCore.pong = function (ping) {
                hanaCore.lastStanzaDate = new Date();
                hanaCore.connection.ping.pong(ping);
                return true;
            };

            hanaCore.registerPongHandler = function () {
                // hanaCore.connection.disco.addFeature(Strophe.NS.PING);
                hanaCore.connection.ping.addPingHandler(hanaCore.pong);
            };

            hanaCore.registerPingHandler = function () {
                hanaCore.registerPongHandler();
                if (hanaCore.ping_interval > 0) {
                    hanaCore.connection.addHandler(function () {
                        /* Handler on each stanza, saves the received date
                         * in order to ping only when needed.
                         */
                        hanaCore.lastStanzaDate = new Date();
                        return true;
                    });
                    hanaCore.connection.addTimedHandler(1000, function () {
                        var now = new Date();
                        if (!hanaCore.lastStanzaDate) {
                            hanaCore.lastStanzaDate = now;
                        }
                        if ((now - hanaCore.lastStanzaDate)/1000 > hanaCore.ping_interval) {
                            return hanaCore.ping();
                        }
                        return true;
                    });
                }
                return true;
            };

            ha_.extend(hana_api, {
                /* We extend the default hanaCore.js API to add a method specific
                 * to this plugin.
                 */
                'ping': function (jid) {
                    hanaCore.ping(jid);
                }
            });

            hanaCore.onConnectedPing = function () {
                // Wrapper so that we can spy on registerPingHandler in tests
                // console_bk.log('CONNECTED PINGHANA ' + hanaCore.connection.jid);
                hanaCore.registerPingHandler();

                hanaCore.foreground();
                hanaCore.registerPingHandler();
            };
            hanaCore.foreground = function () {
                // console_bk.log('SEND FOREGROUND ');
                var pres = ha$pres({ id: Math.random().toString(36).substr(2, 4) });
                hana_api.send(pres);
                // console_bk.log('SEND FOREGROUND ');
                //send foreground
                pres = ha$pres({ id: Math.random().toString(36).substr(2, 4) }).c('foreground');
                hana_api.send(pres);
                
            }
     
            // console_bk.log("REGISTER PING HANA...");
            // hanaCore.on('connected', onConnected);
            // hanaCore.on('reconnected', onConnected);

        }
    });
}));
