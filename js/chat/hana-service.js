// hanaCore.js (A browser based XMPP chat client)
// http://hanaCorejs.org
//
// Copyright (c) 2012-2016, Jan-Carel Brand <jc@opkode.com>
// Licensed under the Mozilla Public License (MPLv2)
//
/*global define */

/* This is a hanaCore.js plugin which add support for application-level pings
 * as specified in XEP-0199 XMPP Ping.
 */
(function (root, factory) {
    define("hana-service", [
        "hana-core",
        "hana-api",
        "hana-util"
    ], factory);
}(this, function (hanaCore, hana_api) {
    "use strict";
    // Strophe methods for building stanzas
    var Strophe = hana_api.env.Strophe;
    // Other necessary globals
    var _ = hana_api.env._,
    ha$iq = hana_api.env.$iq,
    ha$build = hana_api.env.$build,
    ha$msg = hana_api.env.$msg,
    ha$pres = hana_api.env.$pres,
    ha$ = hana_api.env.jQuery;
    
    hana_api.plugins.add('hana-service', {

        initialize: function () {
            //console_bk.log("REGISTER  HANA SERVICE ...");
            /* The initialize function gets called as soon as the plugin is
             * loaded by hanaCore.js's plugin machinery.
             */
            //console_bk.log("REGISTER PING HANA...");
            hanaCore.getIntegration = function (callBack, type) {
                var app = hanaCore.hana_appid;
                var infoUser = JSON.stringify({ "app": app, "gatewayid": type });
                var url = "webclient/get-integration-gateway";
                hanaCore.sendPost(url, infoUser, function (data) {
                    callBack(data);
                });
            };
            hanaCore.webClient = function (uidW, callBack) {
                var uid = uidW;
                var app = hanaCore.hana_appid;
                var infoUser = JSON.stringify({ "uid": uid, "app": app, "type": "webclient", "creater": uid });
                var url = "webclient";
                hanaCore.sendPost(url, infoUser, function (data) {
                    callBack(data);
                });
            };
            hanaCore.getBotInfo = function (callBack) {
                var hana = hanaCore.all_agent.hana.split("_");
                var url = "webclient/get-agent-info";
                var appAgent = hanaCore.hana_appid;
                var infoUser = JSON.stringify({ "useragent": hana[1], "app": appAgent});
                hanaCore.sendPost(url, infoUser, function (data) {
                    callBack(data);
                });
            };
            hanaCore.listRoom = function (uidW, callBack) {
                var infoUser = JSON.stringify({ "uid": uidW, "app": hanaCore.hana_appid, "type": "groupchat" , "creater": uidW});
                var url = "webclient/listroom";
                hanaCore.sendPost(url, infoUser, function (data) {
                    callBack(data);
                });
            };
            hanaCore.saveCustomer = function (name, email, phone, address, callBack) {
                var infoUser = JSON.stringify({"backend_partner_id":hanaCore.partner_id,"name":name,"email":email,"phone":phone,"address":address,"customer_username":hanaCore.all_agent.uid,"source_id":"5"});
                var url = "webclient/savecustomerinfo";
                hanaCore.sendPost(url, infoUser, function (data) {
                    callBack(data);
                });
            };
            hanaCore.getCustomerInfo = function (uidW, callBack) {
                var infoUser = JSON.stringify({ "from": uidW});
                var url = "webclient/getcustomerinfo";
                hanaCore.sendPost(url, infoUser, function (data) {
                    callBack(data);
                });
            };
            hanaCore.getStoreMessage = function (room, beginTime, endTime, sizeMsg, sort, callBack) {
                var infoUser = JSON.stringify({ "room_name": room, "unix_start": beginTime, "unix_end": endTime, "size_msg": sizeMsg, "sort": sort });
                var url = "storemessage/popup";
                hanaCore.sendPost(url, infoUser, function (data) {
                    callBack(data);
                });
            };
            hanaCore.updateActiveContact = function (room,  callBack) {
                var infoUser = JSON.stringify({ "room_name": room });
                var url = "webclient/updateactivecontact";
                hanaCore.sendPost(url, infoUser, function (data) {
                    callBack(data);
                });
            };
            hanaCore.getBlock = function (callBack) {
                var infoUser = JSON.stringify({ "partner_id": hanaCore.partner_id, "type": "default"});
                var url = "webclient/get-list-group-blocks";
                hanaCore.sendPost(url, infoUser, function (data) {
                    callBack(data);
                });
            };
            hanaCore.updateMucRoom = function (infoUser, callBack) {
                var url = "webclient/update-muc-room";
                hanaCore.sendPost(url, infoUser, function (data) {
                    callBack(data);
                });
            };
            hanaCore.updateRoom = function (infoUser, callBack) {
                var url = "webclient/room";
                hanaCore.sendPost(url, infoUser, function (data) {
                    callBack(data);
                });
            };
            hanaCore.updateCustomerInfo = function (room,  callBack) {
                ha$.getJSON('//freegeoip.net/json/?callback', function(data) {
                    var infoUser = JSON.stringify(data);
                    var obj = JSON.parse(infoUser);
                    hanaCore.ip_address = obj.ip;
                    obj['website'] = document.location.href;
                    obj['uid'] = hanaCore.node_jid;
                    obj['app'] = hanaCore.hana_appid;
                    obj['unixtime'] = Date.now();
                    obj['title'] = document.title;
                    obj['pagecount'] = window.history.length;
                    obj['backpage'] = document.referrer;
                    obj['useragent'] = navigator.userAgent;
                    obj['newsession'] = true;
                    infoUser = JSON.stringify(obj);
                    var url = "webclient/customerInfo";
                    try {
                        infoUser = hanaCore.base64Encode(infoUser);
                    } catch (e) {}
                    hanaCore.sendPost(url, infoUser, function (data) {
                        callBack(data);
                    });
                }).fail(function() {});
            };



        }
    });
}));
