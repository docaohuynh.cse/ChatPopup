var config;
if (typeof(require) === 'undefined') {
    /* XXX: Hack to work around r.js's stupid parsing.
     * We want to save the configuration in a variable so that we can reuse it in
     * tests/main.js.
     */
    require = { // jshint ignore:line
        config: function (c) {
            config = c;
        }
    };
}
require.config({
    baseUrl: 'js/chat',
    waitSeconds: 20,
    paths: {
        "requiremore": "requiremore",
        "hanajquery": "hanajquery",
        "hanabackbone": "hanabackbone",
        "hanaunderscore": "hanaunderscore",
        "strophe": "strophe"
    },
    //for traditional libraries not using define() we need to use a shim that allows us to declare them as AMD modules
    shim: {
        "hanabackbone": {
            "deps": ["hanaunderscore", "hanajquery"],
            "exports": "Backbone"  //attaches "Backbone" to the window object
        },
        "hanaunderscore": {
            exports: "_" // exporting _
        }
    },

    deps: ['app'],

    urlArgs: "t=20160320000000" //flusing cache, do not use in production
});

