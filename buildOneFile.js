({
    baseUrl: "js/chat",
    paths: {
        "requiremore": "requiremore",
        "jquery": "jquery",
        "backbone": "backbone",
        "underscore": "underscore"
    },
    shim: {
        "backbone": {
            "deps": ["underscore", "jquery"],
            "exports": "Backbone"  //attaches "Backbone" to the window object
        },
        "underscore": {
            exports: "_" // exporting _
        },
        "jquery": {
            "deps":["requiremore", "libextend","addEventListener"]
        }
    },
    name: "../main",
    mainConfigFile: 'js/main.js',
    // preserveLicenseComments: false,
    out: "built/js/main.js"
})
